/**
 * Generated bundle index. Do not edit.
 */
export * from './public-api';
export { FormatDatesPipe as ɵc } from './lib/pipes/format-dates.pipe';
export { NotificationsService as ɵb } from './lib/services/notifications.service';
export { SocketService as ɵa } from './lib/services/socket.service';
