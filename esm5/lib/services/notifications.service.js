/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NOTIFICATIONS_CONFIG } from './config.token';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "./config.token";
var NotificationsService = /** @class */ (function () {
    function NotificationsService(http, notificationsConfig) {
        this.http = http;
        this.notificationsConfig = notificationsConfig;
    }
    /**
     * @return {?}
     */
    NotificationsService.prototype.getNotifications = /**
     * @return {?}
     */
    function () {
        return this.http.get(this.notificationsConfig.notificationAPI + '/api/user/notifications');
    };
    /**
     * @param {?} data
     * @return {?}
     */
    NotificationsService.prototype.updateNotifications = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        return this.http.put(this.notificationsConfig.notificationAPI + '/api/user/notifications', data).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return res; })));
    };
    /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @return {?}
     */
    NotificationsService.prototype.changeRequestStatus = /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @return {?}
     */
    function (himnarkID, requestID, status, request) {
        /** @type {?} */
        var data = {
            payload: {
                status: status,
                type: request.type,
                details: request.details
            },
            action: 'statusChange'
        };
        return this.http.put(this.notificationsConfig.notificationAPI + '/api/requests/' + requestID, data, {
            headers: new HttpHeaders({ himnark: himnarkID })
        }).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return res; })));
    };
    NotificationsService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NotificationsService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [NOTIFICATIONS_CONFIG,] }] }
    ]; };
    /** @nocollapse */ NotificationsService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NotificationsService_Factory() { return new NotificationsService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.NOTIFICATIONS_CONFIG)); }, token: NotificationsService, providedIn: "root" });
    return NotificationsService;
}());
export { NotificationsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.notificationsConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGhpbW5hcmsvbm90aWZpY2F0aW9ucy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9ub3RpZmljYXRpb25zLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDL0QsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBR3REO0lBSUksOEJBQ1ksSUFBZ0IsRUFDYyxtQkFBd0M7UUFEdEUsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNjLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7SUFDOUUsQ0FBQzs7OztJQUVMLCtDQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxHQUFHLHlCQUF5QixDQUFDLENBQUM7SUFDL0YsQ0FBQzs7Ozs7SUFFRCxrREFBbUI7Ozs7SUFBbkIsVUFBb0IsSUFBSTtRQUNwQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxlQUFlLEdBQUcseUJBQXlCLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsRUFBSCxDQUFHLEVBQUMsQ0FBQyxDQUFDO0lBQzNILENBQUM7Ozs7Ozs7O0lBRUQsa0RBQW1COzs7Ozs7O0lBQW5CLFVBQW9CLFNBQWlCLEVBQUUsU0FBaUIsRUFBRSxNQUFjLEVBQUUsT0FBWTs7WUFDNUUsSUFBSSxHQUFHO1lBQ1QsT0FBTyxFQUFFO2dCQUNMLE1BQU0sUUFBQTtnQkFDTixJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUk7Z0JBQ2xCLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTzthQUMzQjtZQUNELE1BQU0sRUFBRSxjQUFjO1NBQ3pCO1FBQ0QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxHQUFHLGdCQUFnQixHQUFHLFNBQVMsRUFBRSxJQUFJLEVBQUU7WUFDaEcsT0FBTyxFQUFFLElBQUksV0FBVyxDQUFDLEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxDQUFDO1NBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxFQUFILENBQUcsRUFBQyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7Z0JBN0JKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBUFEsVUFBVTtnREFXVixNQUFNLFNBQUMsb0JBQW9COzs7K0JBWnBDO0NBb0NDLEFBOUJELElBOEJDO1NBM0JZLG9CQUFvQjs7Ozs7O0lBRXpCLG9DQUF3Qjs7Ozs7SUFDeEIsbURBQThFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcbmltcG9ydCB7IE5PVElGSUNBVElPTlNfQ09ORklHIH0gZnJvbSAnLi9jb25maWcudG9rZW4nO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uc0NvbmZpZyB9IGZyb20gJy4uL21vZGVscy9jb25maWcnO1xuXG5ASW5qZWN0YWJsZSh7XG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXG59KVxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNTZXJ2aWNlIHtcbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxuICAgICAgICBASW5qZWN0KE5PVElGSUNBVElPTlNfQ09ORklHKSBwcml2YXRlIG5vdGlmaWNhdGlvbnNDb25maWc6IE5vdGlmaWNhdGlvbnNDb25maWdcbiAgICApIHsgfVxuXG4gICAgZ2V0Tm90aWZpY2F0aW9ucygpOiBhbnkge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0aGlzLm5vdGlmaWNhdGlvbnNDb25maWcubm90aWZpY2F0aW9uQVBJICsgJy9hcGkvdXNlci9ub3RpZmljYXRpb25zJyk7XG4gICAgfVxuXG4gICAgdXBkYXRlTm90aWZpY2F0aW9ucyhkYXRhKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAucHV0KHRoaXMubm90aWZpY2F0aW9uc0NvbmZpZy5ub3RpZmljYXRpb25BUEkgKyAnL2FwaS91c2VyL25vdGlmaWNhdGlvbnMnLCBkYXRhKS5waXBlKG1hcChyZXMgPT4gcmVzKSk7XG4gICAgfVxuXG4gICAgY2hhbmdlUmVxdWVzdFN0YXR1cyhoaW1uYXJrSUQ6IHN0cmluZywgcmVxdWVzdElEOiBzdHJpbmcsIHN0YXR1czogc3RyaW5nLCByZXF1ZXN0OiBhbnkpIHtcbiAgICAgICAgY29uc3QgZGF0YSA9IHtcbiAgICAgICAgICAgIHBheWxvYWQ6IHtcbiAgICAgICAgICAgICAgICBzdGF0dXMsXG4gICAgICAgICAgICAgICAgdHlwZTogcmVxdWVzdC50eXBlLFxuICAgICAgICAgICAgICAgIGRldGFpbHM6IHJlcXVlc3QuZGV0YWlsc1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFjdGlvbjogJ3N0YXR1c0NoYW5nZSdcbiAgICAgICAgfTtcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5wdXQodGhpcy5ub3RpZmljYXRpb25zQ29uZmlnLm5vdGlmaWNhdGlvbkFQSSArICcvYXBpL3JlcXVlc3RzLycgKyByZXF1ZXN0SUQsIGRhdGEsIHtcbiAgICAgICAgICAgIGhlYWRlcnM6IG5ldyBIdHRwSGVhZGVycyh7IGhpbW5hcms6IGhpbW5hcmtJRCB9KVxuICAgICAgICB9KS5waXBlKG1hcChyZXMgPT4gcmVzKSk7XG4gICAgfVxufVxuIl19