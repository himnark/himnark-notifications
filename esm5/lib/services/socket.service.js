/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import io from 'socket.io-client';
import { NOTIFICATIONS_CONFIG } from './config.token';
var SocketService = /** @class */ (function () {
    function SocketService(notificationsConfig) {
        this.notificationsConfig = notificationsConfig;
    }
    // // NOTE it will be nice to create connection in constructor
    // // NOTE it will be nice to create connection in constructor
    /**
     * @param {?} auth0ID
     * @return {?}
     */
    SocketService.prototype.createConnection = 
    // // NOTE it will be nice to create connection in constructor
    /**
     * @param {?} auth0ID
     * @return {?}
     */
    function (auth0ID) {
        this.socket = io(this.notificationsConfig.socketAPI, {
            transports: ['websocket'],
            query: { auth0ID: auth0ID }
        });
    };
    // HANDLER
    // HANDLER
    /**
     * @return {?}
     */
    SocketService.prototype.onStatusUpdate = 
    // HANDLER
    /**
     * @return {?}
     */
    function () {
        var _this = this;
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.socket.on('progressUpdate', (/**
             * @param {?} msg
             * @return {?}
             */
            function (msg) {
                observer.next(msg);
            }));
        }));
    };
    /**
     * @return {?}
     */
    SocketService.prototype.onEventAdded = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.socket.on('eventAdded', (/**
             * @param {?} msg
             * @return {?}
             */
            function (msg) {
                observer.next(msg);
            }));
        }));
    };
    /**
     * @return {?}
     */
    SocketService.prototype.onNewNotification = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.socket.on('newNotification', (/**
             * @param {?} msg
             * @return {?}
             */
            function (msg) {
                observer.next(JSON.parse(msg));
            }));
        }));
    };
    /**
     * @param {?} eventData
     * @return {?}
     */
    SocketService.prototype.addEvent = /**
     * @param {?} eventData
     * @return {?}
     */
    function (eventData) {
        this.socket.send('addEvent', eventData);
    };
    SocketService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    SocketService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [NOTIFICATIONS_CONFIG,] }] }
    ]; };
    return SocketService;
}());
export { SocketService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.socket;
    /** @type {?} */
    SocketService.prototype.profile;
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.notificationsConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3NvY2tldC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRWxDLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWxDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXREO0lBS0ksdUJBQzBDLG1CQUF3QztRQUF4Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQy9FLENBQUM7SUFFSiw4REFBOEQ7Ozs7OztJQUM5RCx3Q0FBZ0I7Ozs7OztJQUFoQixVQUFpQixPQUFlO1FBQzVCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUU7WUFDakQsVUFBVSxFQUFFLENBQUMsV0FBVyxDQUFDO1lBQ3pCLEtBQUssRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFO1NBQ3JCLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxVQUFVOzs7OztJQUNWLHNDQUFjOzs7OztJQUFkO1FBQUEsaUJBTUM7UUFMRyxPQUFPLFVBQVUsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQSxRQUFRO1lBQzdCLEtBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGdCQUFnQjs7OztZQUFFLFVBQUEsR0FBRztnQkFDaEMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELG9DQUFZOzs7SUFBWjtRQUFBLGlCQU1DO1FBTEcsT0FBTyxVQUFVLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsUUFBUTtZQUM3QixLQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxZQUFZOzs7O1lBQUUsVUFBQSxHQUFHO2dCQUM1QixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQseUNBQWlCOzs7SUFBakI7UUFBQSxpQkFNQztRQUxHLE9BQU8sVUFBVSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLFFBQVE7WUFDN0IsS0FBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsaUJBQWlCOzs7O1lBQUUsVUFBQyxHQUFHO2dCQUNsQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNuQyxDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxnQ0FBUTs7OztJQUFSLFVBQVMsU0FBYztRQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7Z0JBNUNKLFVBQVU7Ozs7Z0RBTUYsTUFBTSxTQUFDLG9CQUFvQjs7SUF1Q3BDLG9CQUFDO0NBQUEsQUE3Q0QsSUE2Q0M7U0E1Q1ksYUFBYTs7Ozs7O0lBQ3RCLCtCQUEwQjs7SUFDMUIsZ0NBQWE7Ozs7O0lBR1QsNENBQThFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCBpbyBmcm9tICdzb2NrZXQuaW8tY2xpZW50JztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNDb25maWcgfSBmcm9tICcuLi9tb2RlbHMvY29uZmlnJztcbmltcG9ydCB7IE5PVElGSUNBVElPTlNfQ09ORklHIH0gZnJvbSAnLi9jb25maWcudG9rZW4nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU29ja2V0U2VydmljZSB7XG4gICAgcHJpdmF0ZSBzb2NrZXQ6IGlvLlNvY2tldDtcbiAgICBwcm9maWxlOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChOT1RJRklDQVRJT05TX0NPTkZJRykgcHJpdmF0ZSBub3RpZmljYXRpb25zQ29uZmlnOiBOb3RpZmljYXRpb25zQ29uZmlnXG4gICAgKSB7fVxuXG4gICAgLy8gLy8gTk9URSBpdCB3aWxsIGJlIG5pY2UgdG8gY3JlYXRlIGNvbm5lY3Rpb24gaW4gY29uc3RydWN0b3JcbiAgICBjcmVhdGVDb25uZWN0aW9uKGF1dGgwSUQ6IHN0cmluZykge1xuICAgICAgICB0aGlzLnNvY2tldCA9IGlvKHRoaXMubm90aWZpY2F0aW9uc0NvbmZpZy5zb2NrZXRBUEksIHtcbiAgICAgICAgICAgIHRyYW5zcG9ydHM6IFsnd2Vic29ja2V0J10sXG4gICAgICAgICAgICBxdWVyeTogeyBhdXRoMElEIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gSEFORExFUlxuICAgIG9uU3RhdHVzVXBkYXRlKCkge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5jcmVhdGUob2JzZXJ2ZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5zb2NrZXQub24oJ3Byb2dyZXNzVXBkYXRlJywgbXNnID0+IHtcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KG1zZyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgb25FdmVudEFkZGVkKCkge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5jcmVhdGUob2JzZXJ2ZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5zb2NrZXQub24oJ2V2ZW50QWRkZWQnLCBtc2cgPT4ge1xuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQobXNnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBvbk5ld05vdGlmaWNhdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIE9ic2VydmFibGUuY3JlYXRlKG9ic2VydmVyID0+IHtcbiAgICAgICAgICAgIHRoaXMuc29ja2V0Lm9uKCduZXdOb3RpZmljYXRpb24nLCAobXNnKSA9PiB7XG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKG1zZykpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFkZEV2ZW50KGV2ZW50RGF0YTogYW55KSB7XG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoJ2FkZEV2ZW50JywgZXZlbnREYXRhKTtcbiAgICB9XG59XG4iXX0=