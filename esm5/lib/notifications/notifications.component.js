/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { NotificationsService } from '../services/notifications.service';
import { SocketService } from '../services/socket.service';
/** @type {?} */
var moment = moment_;
var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(router, ns, ts, ss) {
        this.router = router;
        this.ns = ns;
        this.ts = ts;
        this.ss = ss;
        this.notifications = [];
        this.badgeNumber = 0;
        this.hideMatBadge = true;
        this.himnarkChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    NotificationsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.ss.createConnection(this.user.sub);
        this.ns.getNotifications().subscribe((/**
         * @param {?} notifications
         * @return {?}
         */
        function (notifications) {
            if (Array.isArray(notifications) && notifications.length) {
                _this.notifications = notifications;
                _this.badgeNumber = notifications.filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return !item.read; })).length;
                if (_this.badgeNumber !== 0) {
                    _this.hideMatBadge = false;
                }
            }
        }));
        this.ss.onNewNotification().subscribe((/**
         * @param {?} notification
         * @return {?}
         */
        function (notification) {
            _this.notifications.unshift(notification);
            _this.badgeNumber += 1;
            _this.hideMatBadge = false;
        }));
    };
    /**
     * @param {?} payload
     * @return {?}
     */
    NotificationsComponent.prototype.getObjForTranslation = /**
     * @param {?} payload
     * @return {?}
     */
    function (payload) {
        /** @type {?} */
        var date = moment(payload.eventDate).format('DD-MM-YYYY');
        if (payload.eventType === 'birthday') {
            date = moment(payload.eventDate).format('DD-MM');
        }
        /** @type {?} */
        var person = '';
        if (payload && payload.person && payload.person.personalInfo) {
            if (payload.person.personalInfo.name) {
                person += payload.person.personalInfo.name;
            }
            if (payload.person.personalInfo.surname) {
                person += ' ' + payload.person.personalInfo.surname;
            }
        }
        return { person: person, date: date };
    };
    /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @param {?} event
     * @return {?}
     */
    NotificationsComponent.prototype.updateRequestStatus = /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @param {?} event
     * @return {?}
     */
    function (himnarkID, requestID, status, request, event) {
        var _this = this;
        event.stopPropagation();
        this.ns.changeRequestStatus(himnarkID, requestID, status, request).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var notification = _this.notifications.find((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.requestObject._id === response.id; }));
            if (notification) {
                notification.requestObject.status = response.status;
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
        }));
    };
    /**
     * @param {?} himnarkId
     * @param {?} payload
     * @return {?}
     */
    NotificationsComponent.prototype.goTo = /**
     * @param {?} himnarkId
     * @param {?} payload
     * @return {?}
     */
    function (himnarkId, payload) {
        // NOTE it is possible to have different paths depends on application
        if (payload) {
            /** @type {?} */
            var type = payload.type;
            /** @type {?} */
            var path = '/' + himnarkId + '/';
            if (type === 'request') {
                path += 'tasks/';
                path += payload.requestType + '/';
                path += payload.requestNumber;
            }
            else if (type === 'event') {
                path += 'employees/';
                path += payload.person._id + '/';
                /** @type {?} */
                var eventType = payload.eventType;
                // NOTE known event types 'birthday', 'trialPeriod', 'termination'
                if (eventType === 'birthday') {
                    path += 'details';
                }
                else {
                    path += 'contracts';
                }
            }
            /** @type {?} */
            var idFromUrl = window.location.pathname.split('/')[1];
            if (himnarkId !== idFromUrl) {
                this.himnarkChanged.emit(himnarkId);
            }
            this.router.navigateByUrl(path);
        }
    };
    /**
     * @return {?}
     */
    NotificationsComponent.prototype.read = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var notReadIDs = this.notifications.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return !item.read; })).map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item._id; }));
        if (notReadIDs.length === 0) {
            return;
        }
        /** @type {?} */
        var data = {
            payload: notReadIDs,
            action: 'read'
        };
        this.ns.updateNotifications(data).subscribe((/**
         * @param {?} updatedIDs
         * @return {?}
         */
        function (updatedIDs) {
            /** @type {?} */
            var readCount = 0;
            if (Array.isArray(updatedIDs)) {
                readCount = updatedIDs.length;
            }
            _this.badgeNumber -= readCount;
            console.log('this.badgeNumbe', _this.badgeNumber, readCount);
            if (_this.badgeNumber === 0) {
                _this.hideMatBadge = true;
            }
            _this.notifications.forEach((/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                if (notReadIDs.includes(item._id)) {
                    item.read = true;
                }
            }));
        }));
    };
    NotificationsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'himnark-notifications',
                    template: "<div class=\"select-app\">\n    <button mat-icon-button [matMenuTriggerFor]=\"appMenu\" (click)=\"read()\">\n        <mat-icon [matBadge]=\"badgeNumber\" matBadgeColor=\"accent\" [matBadgeHidden]=\"hideMatBadge\">\n            notifications\n        </mat-icon>\n    </button>\n    <mat-menu #appMenu=\"matMenu\" class=\"note-menu\">\n        <div class=\"notifications-block\" (click)=\"$event.stopPropagation()\">\n            <div *ngIf=\"!notifications || notifications.length === 0; else notificationList\"\n                fxLayoutAlign=\"center center\" class=\"empty\">\n                <span>{{ \"NOTIFICATIONS.noNotifications\" | translate }}</span>\n            </div>\n            <ng-template #notificationList>\n                <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"notification-list\">\n                    <div *ngFor=\"let item of notifications; let i = index; let l = last\" (click)=\"goTo(item.himnarkID, item.payload)\"\n                        fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"list-item\">\n                        <div *ngIf=\"item.payload && item.payload.type === 'request'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img src=\"{{ item.who?.picture }}\" alt=\"notifier\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ item.who?.name }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.requestType\">\n                                        <ng-template [ngSwitchCase]=\"'annualVacation'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"island\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'timeOff'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"calendar-remove\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'bonus'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"purple\">\n                                                <mat-icon svgIcon=\"gift\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'businessTrip'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-dark\">\n                                                <mat-icon svgIcon=\"bag-carry-on\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'expenseReport'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-light\">\n                                                <mat-icon fxLayoutAlign=\"center center\">receipt</mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span class=\"payload-text\">{{ item.payload.requestTitle }}</span>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"item.payload && item.payload.type === 'event'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img *ngIf=\"item.from === 'himnarkVaspour'\" alt=\"\" src=\"assets/images/vaspour.png\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ \"NOTIFICATIONS.\" + item.who?.name | translate }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.eventType\">\n                                        <ng-template [ngSwitchCase]=\"'birthday'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"red\">cake</mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'termination'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'trialPeriod'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span>\n                                        {{ \"NOTIFICATIONS.\" + item.payload.eventType | translate:getObjForTranslation(item.payload) }}\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                        <mat-divider *ngIf=\"!l\"></mat-divider>\n                    </div>\n                </div>\n            </ng-template>\n        </div>\n    </mat-menu>\n</div>",
                    providers: [SocketService],
                    styles: ["::ng-deep .note-menu{max-width:480px!important}::ng-deep .note-menu .notifications-block{width:440px}::ng-deep .note-menu .notifications-block .empty{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img{height:40px;width:40px;border-radius:50px;overflow:hidden;box-sizing:border-box}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img img{height:100%;width:100%}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body{width:calc(100% - 56px)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .gray-text{color:rgba(0,0,0,.54)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload{background:rgba(0,0,0,.04);border-radius:3px;padding:8px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .orange{color:#fb8c00}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-dark{color:#006064}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-light{color:#009688}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .purple{color:#9575cd}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .red{color:#ef5350}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload-text{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content:hover{cursor:pointer;background-color:#eee}"]
                }] }
    ];
    /** @nocollapse */
    NotificationsComponent.ctorParameters = function () { return [
        { type: Router },
        { type: NotificationsService },
        { type: TranslateService },
        { type: SocketService }
    ]; };
    NotificationsComponent.propDecorators = {
        himnarkChanged: [{ type: Output }],
        user: [{ type: Input }],
        himnarks: [{ type: Input }]
    };
    return NotificationsComponent;
}());
export { NotificationsComponent };
if (false) {
    /** @type {?} */
    NotificationsComponent.prototype.notifications;
    /** @type {?} */
    NotificationsComponent.prototype.badgeNumber;
    /** @type {?} */
    NotificationsComponent.prototype.hideMatBadge;
    /** @type {?} */
    NotificationsComponent.prototype.himnarkChanged;
    /** @type {?} */
    NotificationsComponent.prototype.user;
    /** @type {?} */
    NotificationsComponent.prototype.himnarks;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ns;
    /** @type {?} */
    NotificationsComponent.prototype.ts;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBRWxDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7SUFDckQsTUFBTSxHQUFHLE9BQU87QUFFdEI7SUFlSSxnQ0FDWSxNQUFjLEVBQ2QsRUFBd0IsRUFDekIsRUFBb0IsRUFDbkIsRUFBaUI7UUFIakIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLE9BQUUsR0FBRixFQUFFLENBQXNCO1FBQ3pCLE9BQUUsR0FBRixFQUFFLENBQWtCO1FBQ25CLE9BQUUsR0FBRixFQUFFLENBQWU7UUFYN0Isa0JBQWEsR0FBVSxFQUFFLENBQUM7UUFDMUIsZ0JBQVcsR0FBRyxDQUFDLENBQUM7UUFDaEIsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDVixtQkFBYyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7SUFTMUMsQ0FBQzs7OztJQUVMLHlDQUFROzs7SUFBUjtRQUFBLGlCQWdCQztRQWZHLElBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsYUFBa0I7WUFDcEQsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7Z0JBQ3RELEtBQUksQ0FBQyxhQUFhLEdBQUcsYUFBYSxDQUFDO2dCQUNuQyxLQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsQ0FBQyxNQUFNOzs7O2dCQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFWLENBQVUsRUFBQyxDQUFDLE1BQU0sQ0FBQztnQkFDbkUsSUFBSSxLQUFJLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtvQkFDeEIsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7aUJBQzdCO2FBQ0o7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQSxZQUFZO1lBQzlDLEtBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3pDLEtBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzlCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxxREFBb0I7Ozs7SUFBcEIsVUFBcUIsT0FBWTs7WUFDekIsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQztRQUN6RCxJQUFJLE9BQU8sQ0FBQyxTQUFTLEtBQUssVUFBVSxFQUFFO1lBQ2xDLElBQUksR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUNwRDs7WUFDRyxNQUFNLEdBQUcsRUFBRTtRQUNmLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxNQUFNLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUU7WUFDMUQsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2xDLE1BQU0sSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7YUFDOUM7WUFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRTtnQkFDckMsTUFBTSxJQUFJLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUM7YUFDdkQ7U0FDSjtRQUNELE9BQU8sRUFBRSxNQUFNLFFBQUEsRUFBRSxJQUFJLE1BQUEsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7Ozs7OztJQUVELG9EQUFtQjs7Ozs7Ozs7SUFBbkIsVUFBb0IsU0FBaUIsRUFBRSxTQUFpQixFQUFFLE1BQWMsRUFBRSxPQUFZLEVBQUUsS0FBSztRQUE3RixpQkFTQztRQVJHLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsRUFBRSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLFFBQWE7O2dCQUM3RSxZQUFZLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsS0FBSyxRQUFRLENBQUMsRUFBRSxFQUF0QyxDQUFzQyxFQUFDO1lBQzVGLElBQUksWUFBWSxFQUFFO2dCQUNkLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7YUFDdkQ7UUFDVCxDQUFDOzs7O1FBQUUsVUFBQyxHQUFHO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxxQ0FBSTs7Ozs7SUFBSixVQUFLLFNBQWlCLEVBQUUsT0FBWTtRQUNoQyxxRUFBcUU7UUFDckUsSUFBSSxPQUFPLEVBQUU7O2dCQUNILElBQUksR0FBRyxPQUFPLENBQUMsSUFBSTs7Z0JBQ3JCLElBQUksR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUc7WUFDaEMsSUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUNwQixJQUFJLElBQUksUUFBUSxDQUFDO2dCQUNqQixJQUFJLElBQUksT0FBTyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7Z0JBQ2xDLElBQUksSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDO2FBQ2pDO2lCQUFNLElBQUksSUFBSSxLQUFLLE9BQU8sRUFBRTtnQkFDekIsSUFBSSxJQUFJLFlBQVksQ0FBQztnQkFDckIsSUFBSSxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQzs7b0JBQzNCLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUztnQkFDbkMsa0VBQWtFO2dCQUNsRSxJQUFJLFNBQVMsS0FBSyxVQUFVLEVBQUU7b0JBQzFCLElBQUksSUFBSSxTQUFTLENBQUM7aUJBQ3JCO3FCQUFNO29CQUNILElBQUksSUFBSSxXQUFXLENBQUM7aUJBQ3ZCO2FBQ0o7O2dCQUNLLFNBQVMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hELElBQUksU0FBUyxLQUFLLFNBQVMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7SUFFRCxxQ0FBSTs7O0lBQUo7UUFBQSxpQkF5QkM7O1lBeEJTLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLENBQUMsSUFBSSxDQUFDLElBQUksRUFBVixDQUFVLEVBQUMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsR0FBRyxFQUFSLENBQVEsRUFBQztRQUN0RixJQUFJLFVBQVUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3pCLE9BQU87U0FDVjs7WUFDSyxJQUFJLEdBQUc7WUFDVCxPQUFPLEVBQUUsVUFBVTtZQUNuQixNQUFNLEVBQUUsTUFBTTtTQUNqQjtRQUNELElBQUksQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsVUFBZTs7Z0JBQ3BELFNBQVMsR0FBRyxDQUFDO1lBQ2pCLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDM0IsU0FBUyxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7YUFDakM7WUFDRCxLQUFJLENBQUMsV0FBVyxJQUFJLFNBQVMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLEtBQUksQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDNUQsSUFBSSxLQUFJLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtnQkFDeEIsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7YUFDNUI7WUFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFBLElBQUk7Z0JBQzNCLElBQUksVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2lCQUNwQjtZQUNMLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkF6SEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSx1QkFBdUI7b0JBQ2pDLHMzVUFBNkM7b0JBRTdDLFNBQVMsRUFBRyxDQUFFLGFBQWEsQ0FBRTs7aUJBQ2hDOzs7O2dCQWJRLE1BQU07Z0JBSU4sb0JBQW9CO2dCQUhwQixnQkFBZ0I7Z0JBSWhCLGFBQWE7OztpQ0FjakIsTUFBTTt1QkFDTixLQUFLOzJCQUNMLEtBQUs7O0lBOEdWLDZCQUFDO0NBQUEsQUEzSEQsSUEySEM7U0FySFksc0JBQXNCOzs7SUFFL0IsK0NBQTBCOztJQUMxQiw2Q0FBZ0I7O0lBQ2hCLDhDQUFvQjs7SUFDcEIsZ0RBQThDOztJQUM5QyxzQ0FBbUI7O0lBQ25CLDBDQUFzQjs7Ozs7SUFHbEIsd0NBQXNCOzs7OztJQUN0QixvQ0FBZ0M7O0lBQ2hDLG9DQUEyQjs7Ozs7SUFDM0Isb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcblxuaW1wb3J0IHsgTm90aWZpY2F0aW9uc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9ub3RpZmljYXRpb25zLnNlcnZpY2UnO1xuaW1wb3J0IHsgU29ja2V0U2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3NvY2tldC5zZXJ2aWNlJztcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnaGltbmFyay1ub3RpZmljYXRpb25zJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbm90aWZpY2F0aW9ucy5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbm90aWZpY2F0aW9ucy5jb21wb25lbnQuc2NzcyddLFxuICAgIHByb3ZpZGVyczogIFsgU29ja2V0U2VydmljZSBdXG59KVxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgbm90aWZpY2F0aW9uczogYW55W10gPSBbXTtcbiAgICBiYWRnZU51bWJlciA9IDA7XG4gICAgaGlkZU1hdEJhZGdlID0gdHJ1ZTtcbiAgICBAT3V0cHV0KCkgaGltbmFya0NoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgQElucHV0KCkgdXNlcjogYW55O1xuICAgIEBJbnB1dCgpIGhpbW5hcmtzOiBbXTtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxuICAgICAgICBwcml2YXRlIG5zOiBOb3RpZmljYXRpb25zU2VydmljZSxcbiAgICAgICAgcHVibGljIHRzOiBUcmFuc2xhdGVTZXJ2aWNlLFxuICAgICAgICBwcml2YXRlIHNzOiBTb2NrZXRTZXJ2aWNlXG4gICAgKSB7IH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnNzLmNyZWF0ZUNvbm5lY3Rpb24odGhpcy51c2VyLnN1Yik7XG4gICAgICAgIHRoaXMubnMuZ2V0Tm90aWZpY2F0aW9ucygpLnN1YnNjcmliZSgobm90aWZpY2F0aW9uczogYW55KSA9PiB7XG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheShub3RpZmljYXRpb25zKSAmJiBub3RpZmljYXRpb25zLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucyA9IG5vdGlmaWNhdGlvbnM7XG4gICAgICAgICAgICAgICAgdGhpcy5iYWRnZU51bWJlciA9IG5vdGlmaWNhdGlvbnMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0ucmVhZCkubGVuZ3RoO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmJhZGdlTnVtYmVyICE9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaGlkZU1hdEJhZGdlID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5zcy5vbk5ld05vdGlmaWNhdGlvbigpLnN1YnNjcmliZShub3RpZmljYXRpb24gPT4ge1xuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLnVuc2hpZnQobm90aWZpY2F0aW9uKTtcbiAgICAgICAgICAgIHRoaXMuYmFkZ2VOdW1iZXIgKz0gMTtcbiAgICAgICAgICAgIHRoaXMuaGlkZU1hdEJhZGdlID0gZmFsc2U7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGdldE9iakZvclRyYW5zbGF0aW9uKHBheWxvYWQ6IGFueSkge1xuICAgICAgICBsZXQgZGF0ZSA9IG1vbWVudChwYXlsb2FkLmV2ZW50RGF0ZSkuZm9ybWF0KCdERC1NTS1ZWVlZJyk7XG4gICAgICAgIGlmIChwYXlsb2FkLmV2ZW50VHlwZSA9PT0gJ2JpcnRoZGF5Jykge1xuICAgICAgICAgICAgZGF0ZSA9IG1vbWVudChwYXlsb2FkLmV2ZW50RGF0ZSkuZm9ybWF0KCdERC1NTScpO1xuICAgICAgICB9XG4gICAgICAgIGxldCBwZXJzb24gPSAnJztcbiAgICAgICAgaWYgKHBheWxvYWQgJiYgcGF5bG9hZC5wZXJzb24gJiYgcGF5bG9hZC5wZXJzb24ucGVyc29uYWxJbmZvKSB7XG4gICAgICAgICAgICBpZiAocGF5bG9hZC5wZXJzb24ucGVyc29uYWxJbmZvLm5hbWUpIHtcbiAgICAgICAgICAgICAgICBwZXJzb24gKz0gcGF5bG9hZC5wZXJzb24ucGVyc29uYWxJbmZvLm5hbWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAocGF5bG9hZC5wZXJzb24ucGVyc29uYWxJbmZvLnN1cm5hbWUpIHtcbiAgICAgICAgICAgICAgICBwZXJzb24gKz0gJyAnICsgcGF5bG9hZC5wZXJzb24ucGVyc29uYWxJbmZvLnN1cm5hbWU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHsgcGVyc29uLCBkYXRlIH07XG4gICAgfVxuXG4gICAgdXBkYXRlUmVxdWVzdFN0YXR1cyhoaW1uYXJrSUQ6IHN0cmluZywgcmVxdWVzdElEOiBzdHJpbmcsIHN0YXR1czogc3RyaW5nLCByZXF1ZXN0OiBhbnksIGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB0aGlzLm5zLmNoYW5nZVJlcXVlc3RTdGF0dXMoaGltbmFya0lELCByZXF1ZXN0SUQsIHN0YXR1cywgcmVxdWVzdCkuc3Vic2NyaWJlKChyZXNwb25zZTogYW55KSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3Qgbm90aWZpY2F0aW9uID0gdGhpcy5ub3RpZmljYXRpb25zLmZpbmQoaXRlbSA9PiBpdGVtLnJlcXVlc3RPYmplY3QuX2lkID09PSByZXNwb25zZS5pZCk7XG4gICAgICAgICAgICAgICAgaWYgKG5vdGlmaWNhdGlvbikge1xuICAgICAgICAgICAgICAgICAgICBub3RpZmljYXRpb24ucmVxdWVzdE9iamVjdC5zdGF0dXMgPSByZXNwb25zZS5zdGF0dXM7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICB9LCAoZXJyKSA9PiB7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGdvVG8oaGltbmFya0lkOiBzdHJpbmcsIHBheWxvYWQ6IGFueSkge1xuICAgICAgICAvLyBOT1RFIGl0IGlzIHBvc3NpYmxlIHRvIGhhdmUgZGlmZmVyZW50IHBhdGhzIGRlcGVuZHMgb24gYXBwbGljYXRpb25cbiAgICAgICAgaWYgKHBheWxvYWQpIHtcbiAgICAgICAgICAgIGNvbnN0IHR5cGUgPSBwYXlsb2FkLnR5cGU7XG4gICAgICAgICAgICBsZXQgcGF0aCA9ICcvJyArIGhpbW5hcmtJZCArICcvJztcbiAgICAgICAgICAgIGlmICh0eXBlID09PSAncmVxdWVzdCcpIHtcbiAgICAgICAgICAgICAgICBwYXRoICs9ICd0YXNrcy8nO1xuICAgICAgICAgICAgICAgIHBhdGggKz0gcGF5bG9hZC5yZXF1ZXN0VHlwZSArICcvJztcbiAgICAgICAgICAgICAgICBwYXRoICs9IHBheWxvYWQucmVxdWVzdE51bWJlcjtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZSA9PT0gJ2V2ZW50Jykge1xuICAgICAgICAgICAgICAgIHBhdGggKz0gJ2VtcGxveWVlcy8nO1xuICAgICAgICAgICAgICAgIHBhdGggKz0gcGF5bG9hZC5wZXJzb24uX2lkICsgJy8nO1xuICAgICAgICAgICAgICAgIGNvbnN0IGV2ZW50VHlwZSA9IHBheWxvYWQuZXZlbnRUeXBlO1xuICAgICAgICAgICAgICAgIC8vIE5PVEUga25vd24gZXZlbnQgdHlwZXMgJ2JpcnRoZGF5JywgJ3RyaWFsUGVyaW9kJywgJ3Rlcm1pbmF0aW9uJ1xuICAgICAgICAgICAgICAgIGlmIChldmVudFR5cGUgPT09ICdiaXJ0aGRheScpIHtcbiAgICAgICAgICAgICAgICAgICAgcGF0aCArPSAnZGV0YWlscyc7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcGF0aCArPSAnY29udHJhY3RzJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjb25zdCBpZEZyb21VcmwgPSB3aW5kb3cubG9jYXRpb24ucGF0aG5hbWUuc3BsaXQoJy8nKVsxXTtcbiAgICAgICAgICAgIGlmIChoaW1uYXJrSWQgIT09IGlkRnJvbVVybCkge1xuICAgICAgICAgICAgICAgIHRoaXMuaGltbmFya0NoYW5nZWQuZW1pdChoaW1uYXJrSWQpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChwYXRoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHJlYWQoKSB7XG4gICAgICAgIGNvbnN0IG5vdFJlYWRJRHMgPSB0aGlzLm5vdGlmaWNhdGlvbnMuZmlsdGVyKGl0ZW0gPT4gIWl0ZW0ucmVhZCkubWFwKGl0ZW0gPT4gaXRlbS5faWQpO1xuICAgICAgICBpZiAobm90UmVhZElEcy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBkYXRhID0ge1xuICAgICAgICAgICAgcGF5bG9hZDogbm90UmVhZElEcyxcbiAgICAgICAgICAgIGFjdGlvbjogJ3JlYWQnXG4gICAgICAgIH07XG4gICAgICAgIHRoaXMubnMudXBkYXRlTm90aWZpY2F0aW9ucyhkYXRhKS5zdWJzY3JpYmUoKHVwZGF0ZWRJRHM6IGFueSkgPT4ge1xuICAgICAgICAgICAgbGV0IHJlYWRDb3VudCA9IDA7XG4gICAgICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh1cGRhdGVkSURzKSkge1xuICAgICAgICAgICAgICAgIHJlYWRDb3VudCA9IHVwZGF0ZWRJRHMubGVuZ3RoO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5iYWRnZU51bWJlciAtPSByZWFkQ291bnQ7XG4gICAgICAgICAgICBjb25zb2xlLmxvZygndGhpcy5iYWRnZU51bWJlJywgdGhpcy5iYWRnZU51bWJlciwgcmVhZENvdW50KTtcbiAgICAgICAgICAgIGlmICh0aGlzLmJhZGdlTnVtYmVyID09PSAwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5oaWRlTWF0QmFkZ2UgPSB0cnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zLmZvckVhY2goaXRlbSA9PiB7XG4gICAgICAgICAgICAgICAgaWYgKG5vdFJlYWRJRHMuaW5jbHVkZXMoaXRlbS5faWQpKSB7XG4gICAgICAgICAgICAgICAgICAgIGl0ZW0ucmVhZCA9IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxufVxuIl19