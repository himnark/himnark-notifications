/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import * as moment_ from 'moment';
/** @type {?} */
var moment = moment_;
var FormatDatesPipe = /** @class */ (function () {
    function FormatDatesPipe() {
    }
    /**
     * @param {?} dates
     * @param {?} monthFormat
     * @param {?} typeCase
     * @param {?} lang
     * @return {?}
     */
    FormatDatesPipe.prototype.transform = /**
     * @param {?} dates
     * @param {?} monthFormat
     * @param {?} typeCase
     * @param {?} lang
     * @return {?}
     */
    function (dates, monthFormat, typeCase, lang) {
        /** @type {?} */
        var date = '';
        /** @type {?} */
        var firstDay;
        /** @type {?} */
        var lastDay;
        /** @type {?} */
        var localMoment = moment;
        localMoment.locale(lang);
        dates = dates.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item; }));
        if (dates && dates.length) {
            if (localMoment(dates[0], 'DD/MM/YYYY').isValid()) {
                firstDay = localMoment(dates[0], 'DD/MM/YYYY');
                lastDay = localMoment(dates[dates.length - 1], 'DD/MM/YYYY');
            }
            else {
                firstDay = localMoment(dates[0]);
                lastDay = localMoment(dates[dates.length - 1]);
            }
            if (dates.length === 1) {
                /** @type {?} */
                var format = 'DD';
                if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                    format = format + ', YYYY';
                }
                /** @type {?} */
                var month = firstDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    month = month.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    month = month.toUpperCase();
                }
                else {
                    month = month.charAt(0).toUpperCase() + month.slice(1);
                }
                date = month + ' ' + firstDay.format(format);
            }
            else {
                /** @type {?} */
                var firstMonth = firstDay.format(monthFormat);
                /** @type {?} */
                var lastMonth = lastDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    firstMonth = firstMonth.toLowerCase();
                    lastMonth = lastMonth.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    firstMonth = firstMonth.toUpperCase();
                    lastMonth = lastMonth.toUpperCase();
                }
                else {
                    firstMonth = firstMonth.charAt(0).toUpperCase() + firstMonth.slice(1);
                    lastMonth = lastMonth.charAt(0).toUpperCase() + lastMonth.slice(1);
                }
                if (firstDay.format('YYYY') !== lastDay.format('YYYY')) {
                    /** @type {?} */
                    var format = 'DD';
                    /** @type {?} */
                    var formatLast = 'DD';
                    if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                        format = format + ', YYYY';
                    }
                    if (lastDay.format('YYYY') !== localMoment().format('YYYY')) {
                        formatLast = formatLast + ', YYYY';
                    }
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                }
                else if (firstDay.format('YYYY') === lastDay.format('YYYY')) {
                    /** @type {?} */
                    var format = 'DD';
                    /** @type {?} */
                    var formatLast = 'DD';
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                    if (firstDay.format(monthFormat) === lastDay.format(monthFormat)) {
                        format = 'DD';
                        formatLast = 'DD';
                        if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                            formatLast = formatLast + ', YYYY';
                        }
                        date = firstMonth + ' ' + firstDay.format(format) + '-' + lastDay.format(formatLast);
                    }
                }
            }
        }
        // NOTE Seems that this is not the great way to solve an issue related to moment local
        // anyway, so far so good https://momentjs.com/docs/#/i18n/instance-locale/
        localMoment.locale('en');
        return date;
    };
    FormatDatesPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'formatDates'
                },] }
    ];
    /** @nocollapse */
    FormatDatesPipe.ctorParameters = function () { return []; };
    return FormatDatesPipe;
}());
export { FormatDatesPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LWRhdGVzLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2Zvcm1hdC1kYXRlcy5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQzs7SUFDNUIsTUFBTSxHQUFHLE9BQU87QUFFdEI7SUFLSTtJQUNBLENBQUM7Ozs7Ozs7O0lBRUQsbUNBQVM7Ozs7Ozs7SUFBVCxVQUFVLEtBQWlCLEVBQUUsV0FBbUIsRUFBRSxRQUFnQixFQUFFLElBQVk7O1lBQ3hFLElBQUksR0FBRyxFQUFFOztZQUNULFFBQVE7O1lBQ1IsT0FBTzs7WUFDTCxXQUFXLEdBQUcsTUFBTTtRQUMxQixXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTTs7OztRQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxFQUFKLENBQUksRUFBQyxDQUFDO1FBQ25DLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDdkIsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUMvQyxRQUFRLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFDL0MsT0FBTyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUNoRTtpQkFBTTtnQkFDSCxRQUFRLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxPQUFPLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEQ7WUFDRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOztvQkFDaEIsTUFBTSxHQUFHLElBQUk7Z0JBQ2pCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzFELE1BQU0sR0FBRyxNQUFNLEdBQUcsUUFBUSxDQUFDO2lCQUM5Qjs7b0JBQ0csS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO2dCQUN4QyxJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7b0JBQzFCLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQy9CO3FCQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtvQkFDakMsS0FBSyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDL0I7cUJBQU07b0JBQ0gsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUQ7Z0JBQ0QsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNoRDtpQkFBTTs7b0JBQ0MsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDOztvQkFDekMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO2dCQUMzQyxJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7b0JBQzFCLFVBQVUsR0FBRyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3RDLFNBQVMsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3ZDO3FCQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtvQkFDakMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDdEMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDdkM7cUJBQU07b0JBQ0gsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdEUsU0FBUyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEU7Z0JBQ0QsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7O3dCQUNoRCxNQUFNLEdBQUcsSUFBSTs7d0JBQ2IsVUFBVSxHQUFHLElBQUk7b0JBQ3JCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQzFELE1BQU0sR0FBRyxNQUFNLEdBQUcsUUFBUSxDQUFDO3FCQUM5QjtvQkFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUN6RCxVQUFVLEdBQUcsVUFBVSxHQUFHLFFBQVEsQ0FBQztxQkFDdEM7b0JBQ0QsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUMxRztxQkFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRTs7d0JBQ3ZELE1BQU0sR0FBRyxJQUFJOzt3QkFDYixVQUFVLEdBQUcsSUFBSTtvQkFDckIsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN2RyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRTt3QkFDOUQsTUFBTSxHQUFHLElBQUksQ0FBQzt3QkFDZCxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUNsQixJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUMxRCxVQUFVLEdBQUcsVUFBVSxHQUFHLFFBQVEsQ0FBQzt5QkFDdEM7d0JBQ0QsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDeEY7aUJBQ0o7YUFDSjtTQUNKO1FBQ0Qsc0ZBQXNGO1FBQ3RGLDJFQUEyRTtRQUMzRSxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7O2dCQS9FSixJQUFJLFNBQUM7b0JBQ0YsSUFBSSxFQUFFLGFBQWE7aUJBQ3RCOzs7O0lBOEVELHNCQUFDO0NBQUEsQUFoRkQsSUFnRkM7U0E3RVksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XG5cbkBQaXBlKHtcbiAgICBuYW1lOiAnZm9ybWF0RGF0ZXMnXG59KVxuZXhwb3J0IGNsYXNzIEZvcm1hdERhdGVzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgfVxuXG4gICAgdHJhbnNmb3JtKGRhdGVzOiBBcnJheTxhbnk+LCBtb250aEZvcm1hdDogc3RyaW5nLCB0eXBlQ2FzZTogc3RyaW5nLCBsYW5nOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBsZXQgZGF0ZSA9ICcnO1xuICAgICAgICBsZXQgZmlyc3REYXk7XG4gICAgICAgIGxldCBsYXN0RGF5O1xuICAgICAgICBjb25zdCBsb2NhbE1vbWVudCA9IG1vbWVudDtcbiAgICAgICAgbG9jYWxNb21lbnQubG9jYWxlKGxhbmcpO1xuICAgICAgICBkYXRlcyA9IGRhdGVzLmZpbHRlcihpdGVtID0+IGl0ZW0pO1xuICAgICAgICBpZiAoZGF0ZXMgJiYgZGF0ZXMubGVuZ3RoKSB7XG4gICAgICAgICAgICBpZiAobG9jYWxNb21lbnQoZGF0ZXNbMF0sICdERC9NTS9ZWVlZJykuaXNWYWxpZCgpKSB7XG4gICAgICAgICAgICAgICAgZmlyc3REYXkgPSBsb2NhbE1vbWVudChkYXRlc1swXSwgJ0REL01NL1lZWVknKTtcbiAgICAgICAgICAgICAgICBsYXN0RGF5ID0gbG9jYWxNb21lbnQoZGF0ZXNbZGF0ZXMubGVuZ3RoIC0gMV0sICdERC9NTS9ZWVlZJyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGZpcnN0RGF5ID0gbG9jYWxNb21lbnQoZGF0ZXNbMF0pO1xuICAgICAgICAgICAgICAgIGxhc3REYXkgPSBsb2NhbE1vbWVudChkYXRlc1tkYXRlcy5sZW5ndGggLSAxXSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZiAoZGF0ZXMubGVuZ3RoID09PSAxKSB7XG4gICAgICAgICAgICAgICAgbGV0IGZvcm1hdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgaWYgKGZpcnN0RGF5LmZvcm1hdCgnWVlZWScpICE9PSBsb2NhbE1vbWVudCgpLmZvcm1hdCgnWVlZWScpKSB7XG4gICAgICAgICAgICAgICAgICAgIGZvcm1hdCA9IGZvcm1hdCArICcsIFlZWVknO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBsZXQgbW9udGggPSBmaXJzdERheS5mb3JtYXQobW9udGhGb3JtYXQpO1xuICAgICAgICAgICAgICAgIGlmICh0eXBlQ2FzZSA9PT0gJ2xvd2VyY2FzZScpIHtcbiAgICAgICAgICAgICAgICAgICAgbW9udGggPSBtb250aC50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZUNhc2UgPT09ICd1cHBlcmNhc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vbnRoID0gbW9udGgudG9VcHBlckNhc2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBtb250aCA9IG1vbnRoLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgbW9udGguc2xpY2UoMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGRhdGUgPSBtb250aCArICcgJyArIGZpcnN0RGF5LmZvcm1hdChmb3JtYXQpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBsZXQgZmlyc3RNb250aCA9IGZpcnN0RGF5LmZvcm1hdChtb250aEZvcm1hdCk7XG4gICAgICAgICAgICAgICAgbGV0IGxhc3RNb250aCA9IGxhc3REYXkuZm9ybWF0KG1vbnRoRm9ybWF0KTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZUNhc2UgPT09ICdsb3dlcmNhc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgIGZpcnN0TW9udGggPSBmaXJzdE1vbnRoLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgICAgIGxhc3RNb250aCA9IGxhc3RNb250aC50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodHlwZUNhc2UgPT09ICd1cHBlcmNhc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgIGZpcnN0TW9udGggPSBmaXJzdE1vbnRoLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgICAgIGxhc3RNb250aCA9IGxhc3RNb250aC50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGZpcnN0TW9udGggPSBmaXJzdE1vbnRoLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgZmlyc3RNb250aC5zbGljZSgxKTtcbiAgICAgICAgICAgICAgICAgICAgbGFzdE1vbnRoID0gbGFzdE1vbnRoLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgbGFzdE1vbnRoLnNsaWNlKDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBpZiAoZmlyc3REYXkuZm9ybWF0KCdZWVlZJykgIT09IGxhc3REYXkuZm9ybWF0KCdZWVlZJykpIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZvcm1hdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgICAgIGxldCBmb3JtYXRMYXN0ID0gJ0REJztcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpcnN0RGF5LmZvcm1hdCgnWVlZWScpICE9PSBsb2NhbE1vbWVudCgpLmZvcm1hdCgnWVlZWScpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyAnLCBZWVlZJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBpZiAobGFzdERheS5mb3JtYXQoJ1lZWVknKSAhPT0gbG9jYWxNb21lbnQoKS5mb3JtYXQoJ1lZWVknKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0TGFzdCA9IGZvcm1hdExhc3QgKyAnLCBZWVlZJztcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBkYXRlID0gZmlyc3RNb250aCArICcgJyArIGZpcnN0RGF5LmZvcm1hdChmb3JtYXQpICsgJy0nICsgbGFzdE1vbnRoICsgJyAnICsgbGFzdERheS5mb3JtYXQoZm9ybWF0TGFzdCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChmaXJzdERheS5mb3JtYXQoJ1lZWVknKSA9PT0gbGFzdERheS5mb3JtYXQoJ1lZWVknKSkge1xuICAgICAgICAgICAgICAgICAgICBsZXQgZm9ybWF0ID0gJ0REJztcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZvcm1hdExhc3QgPSAnREQnO1xuICAgICAgICAgICAgICAgICAgICBkYXRlID0gZmlyc3RNb250aCArICcgJyArIGZpcnN0RGF5LmZvcm1hdChmb3JtYXQpICsgJy0nICsgbGFzdE1vbnRoICsgJyAnICsgbGFzdERheS5mb3JtYXQoZm9ybWF0TGFzdCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChmaXJzdERheS5mb3JtYXQobW9udGhGb3JtYXQpID09PSBsYXN0RGF5LmZvcm1hdChtb250aEZvcm1hdCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXRMYXN0ID0gJ0REJztcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmaXJzdERheS5mb3JtYXQoJ1lZWVknKSAhPT0gbG9jYWxNb21lbnQoKS5mb3JtYXQoJ1lZWVknKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdExhc3QgPSBmb3JtYXRMYXN0ICsgJywgWVlZWSc7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlID0gZmlyc3RNb250aCArICcgJyArIGZpcnN0RGF5LmZvcm1hdChmb3JtYXQpICsgJy0nICsgbGFzdERheS5mb3JtYXQoZm9ybWF0TGFzdCk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgLy8gTk9URSBTZWVtcyB0aGF0IHRoaXMgaXMgbm90IHRoZSBncmVhdCB3YXkgdG8gc29sdmUgYW4gaXNzdWUgcmVsYXRlZCB0byBtb21lbnQgbG9jYWxcbiAgICAgICAgLy8gYW55d2F5LCBzbyBmYXIgc28gZ29vZCBodHRwczovL21vbWVudGpzLmNvbS9kb2NzLyMvaTE4bi9pbnN0YW5jZS1sb2NhbGUvXG4gICAgICAgIGxvY2FsTW9tZW50LmxvY2FsZSgnZW4nKTtcbiAgICAgICAgcmV0dXJuIGRhdGU7XG4gICAgfVxufVxuIl19