/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NotificationsComponent } from './notifications/notifications.component';
import { Subject } from 'rxjs';
import { NotificationsService } from './services/notifications.service';
import { SocketService } from './services/socket.service';
import { FormatDatesPipe } from './pipes/format-dates.pipe';
import { NOTIFICATIONS_CONFIG } from './services/config.token';
// REVIEW is this required?
var 
// REVIEW is this required?
Loader = /** @class */ (function () {
    function Loader() {
        this.translations = new Subject();
        this.$translations = this.translations.asObservable();
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    Loader.prototype.getTranslation = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        return this.$translations;
    };
    return Loader;
}());
// REVIEW is this required?
export { Loader };
if (false) {
    /**
     * @type {?}
     * @private
     */
    Loader.prototype.translations;
    /** @type {?} */
    Loader.prototype.$translations;
}
/**
 * @return {?}
 */
export function LoaderFactory() {
    return new Loader();
}
// @dynamic
var NotificationsModule = /** @class */ (function () {
    function NotificationsModule() {
    }
    NotificationsModule.decorators = [
        { type: NgModule, args: [{
                    imports: [CommonModule,
                        MatBadgeModule,
                        MatButtonModule,
                        MatMenuModule,
                        MatIconModule,
                        MatDividerModule,
                        FlexLayoutModule,
                        TranslateModule.forChild({
                            loader: {
                                provide: TranslateLoader,
                                // REVIEW  is this required?
                                useFactory: LoaderFactory
                            }
                        })],
                    declarations: [NotificationsComponent,
                        FormatDatesPipe],
                    exports: [NotificationsComponent,
                        FormatDatesPipe],
                    providers: [
                        NotificationsService,
                        SocketService,
                        {
                            provide: NOTIFICATIONS_CONFIG,
                            useFactory: ConfigFactory
                        }
                    ]
                },] }
    ];
    return NotificationsModule;
}());
export { NotificationsModule };
if (false) {
    /** @type {?} */
    NotificationsModule.config;
}
/**
 * @return {?}
 */
export function ConfigFactory() {
    return NotificationsModule.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxlQUFlLEVBQ3BCLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWpELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFL0IsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRTFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUc1RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFHL0Q7OztJQUFBO1FBQ1ksaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLGtCQUFhLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUlyRCxDQUFDOzs7OztJQUhHLCtCQUFjOzs7O0lBQWQsVUFBZSxJQUFZO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUM1QixDQUFDO0lBQ0wsYUFBQztBQUFELENBQUMsQUFORCxJQU1DOzs7Ozs7OztJQUxHLDhCQUFxQzs7SUFDckMsK0JBQWlEOzs7OztBQU1yRCxNQUFNLFVBQVUsYUFBYTtJQUN6QixPQUFPLElBQUksTUFBTSxFQUFFLENBQUM7QUFDeEIsQ0FBQzs7QUFHRDtJQUFBO0lBNkJBLENBQUM7O2dCQTdCQSxRQUFRLFNBQUM7b0JBQ04sT0FBTyxFQUFFLENBQUMsWUFBWTt3QkFDbEIsY0FBYzt3QkFDZCxlQUFlO3dCQUNmLGFBQWE7d0JBQ2IsYUFBYTt3QkFDYixnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsZUFBZSxDQUFDLFFBQVEsQ0FBQzs0QkFDckIsTUFBTSxFQUFFO2dDQUNKLE9BQU8sRUFBRSxlQUFlOztnQ0FDeEIsVUFBVSxFQUFFLGFBQWE7NkJBQzVCO3lCQUNKLENBQUMsQ0FBQztvQkFDUCxZQUFZLEVBQUUsQ0FBQyxzQkFBc0I7d0JBQ2pDLGVBQWUsQ0FBQztvQkFDcEIsT0FBTyxFQUFFLENBQUMsc0JBQXNCO3dCQUM1QixlQUFlLENBQUM7b0JBQ3BCLFNBQVMsRUFBRTt3QkFDUCxvQkFBb0I7d0JBQ3BCLGFBQWE7d0JBQ2I7NEJBQ0ksT0FBTyxFQUFFLG9CQUFvQjs0QkFDN0IsVUFBVSxFQUFFLGFBQWE7eUJBQzVCO3FCQUNKO2lCQUNKOztJQUdELDBCQUFDO0NBQUEsQUE3QkQsSUE2QkM7U0FGWSxtQkFBbUI7OztJQUM1QiwyQkFBbUM7Ozs7O0FBR3ZDLE1BQU0sVUFBVSxhQUFhO0lBQ3pCLE9BQU8sbUJBQW1CLENBQUMsTUFBTSxDQUFDO0FBQ3RDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbic7XG5pbXBvcnQgeyBNYXRCYWRnZU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2JhZGdlJztcbmltcG9ydCB7IE1hdE1lbnVNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9tZW51JztcbmltcG9ydCB7IE1hdEljb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9pY29uJztcbmltcG9ydCB7IE1hdERpdmlkZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaXZpZGVyJztcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XG5cbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSxcbiAgICBUcmFuc2xhdGVMb2FkZXIgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcblxuaW1wb3J0IHsgTm90aWZpY2F0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vbm90aWZpY2F0aW9ucy9ub3RpZmljYXRpb25zLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9ub3RpZmljYXRpb25zLnNlcnZpY2UnO1xuaW1wb3J0IHsgU29ja2V0U2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvc29ja2V0LnNlcnZpY2UnO1xuXG5pbXBvcnQgeyBGb3JtYXREYXRlc1BpcGUgfSBmcm9tICcuL3BpcGVzL2Zvcm1hdC1kYXRlcy5waXBlJztcblxuaW1wb3J0IHsgTm90aWZpY2F0aW9uc0NvbmZpZyB9IGZyb20gJy4vbW9kZWxzL2NvbmZpZyc7XG5pbXBvcnQgeyBOT1RJRklDQVRJT05TX0NPTkZJRyB9IGZyb20gJy4vc2VydmljZXMvY29uZmlnLnRva2VuJztcblxuLy8gUkVWSUVXIGlzIHRoaXMgcmVxdWlyZWQ/XG5leHBvcnQgY2xhc3MgTG9hZGVyIGltcGxlbWVudHMgVHJhbnNsYXRlTG9hZGVyIHtcbiAgICBwcml2YXRlIHRyYW5zbGF0aW9ucyA9IG5ldyBTdWJqZWN0KCk7XG4gICAgJHRyYW5zbGF0aW9ucyA9IHRoaXMudHJhbnNsYXRpb25zLmFzT2JzZXJ2YWJsZSgpO1xuICAgIGdldFRyYW5zbGF0aW9uKGxhbmc6IHN0cmluZykge1xuICAgICAgcmV0dXJuIHRoaXMuJHRyYW5zbGF0aW9ucztcbiAgICB9XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBMb2FkZXJGYWN0b3J5KCkge1xuICAgIHJldHVybiBuZXcgTG9hZGVyKCk7XG59XG5cbi8vIEBkeW5hbWljXG5ATmdNb2R1bGUoe1xuICAgIGltcG9ydHM6IFtDb21tb25Nb2R1bGUsXG4gICAgICAgIE1hdEJhZGdlTW9kdWxlLFxuICAgICAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgICAgIE1hdE1lbnVNb2R1bGUsXG4gICAgICAgIE1hdEljb25Nb2R1bGUsXG4gICAgICAgIE1hdERpdmlkZXJNb2R1bGUsXG4gICAgICAgIEZsZXhMYXlvdXRNb2R1bGUsXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCh7XG4gICAgICAgICAgICBsb2FkZXI6IHtcbiAgICAgICAgICAgICAgICBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsIC8vIFJFVklFVyAgaXMgdGhpcyByZXF1aXJlZD9cbiAgICAgICAgICAgICAgICB1c2VGYWN0b3J5OiBMb2FkZXJGYWN0b3J5XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pXSxcbiAgICBkZWNsYXJhdGlvbnM6IFtOb3RpZmljYXRpb25zQ29tcG9uZW50LFxuICAgICAgICBGb3JtYXREYXRlc1BpcGVdLFxuICAgIGV4cG9ydHM6IFtOb3RpZmljYXRpb25zQ29tcG9uZW50LFxuICAgICAgICBGb3JtYXREYXRlc1BpcGVdLFxuICAgIHByb3ZpZGVyczogW1xuICAgICAgICBOb3RpZmljYXRpb25zU2VydmljZSxcbiAgICAgICAgU29ja2V0U2VydmljZSxcbiAgICAgICAge1xuICAgICAgICAgICAgcHJvdmlkZTogTk9USUZJQ0FUSU9OU19DT05GSUcsXG4gICAgICAgICAgICB1c2VGYWN0b3J5OiBDb25maWdGYWN0b3J5XG4gICAgICAgIH1cbiAgICBdXG59KVxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNNb2R1bGUge1xuICAgIHN0YXRpYyBjb25maWc6IE5vdGlmaWNhdGlvbnNDb25maWc7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBDb25maWdGYWN0b3J5KCkge1xuICAgIHJldHVybiBOb3RpZmljYXRpb25zTW9kdWxlLmNvbmZpZztcbn1cbiJdfQ==