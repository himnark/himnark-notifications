/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of notifications
 */
export {} from './lib/models/config';
export { NOTIFICATIONS_CONFIG } from './lib/services/config.token';
export { LoaderFactory, ConfigFactory, Loader, NotificationsModule } from './lib/notifications.module';
export { NotificationsComponent } from './lib/notifications/notifications.component';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BoaW1uYXJrL25vdGlmaWNhdGlvbnMvIiwic291cmNlcyI6WyJwdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFHQSxlQUFjLHFCQUFxQixDQUFDO0FBQ3BDLHFDQUFjLDZCQUE2QixDQUFDO0FBQzVDLDBFQUFjLDRCQUE0QixDQUFDO0FBQzNDLHVDQUFjLDZDQUE2QyxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBub3RpZmljYXRpb25zXG4gKi9cbmV4cG9ydCAqIGZyb20gJy4vbGliL21vZGVscy9jb25maWcnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvc2VydmljZXMvY29uZmlnLnRva2VuJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL25vdGlmaWNhdGlvbnMubW9kdWxlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQnO1xuIl19