(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common'), require('@angular/material/button'), require('@angular/material/badge'), require('@angular/material/menu'), require('@angular/material/icon'), require('@angular/material/divider'), require('@angular/flex-layout'), require('@ngx-translate/core'), require('@angular/router'), require('moment'), require('@angular/common/http'), require('rxjs/operators'), require('rxjs'), require('socket.io-client')) :
    typeof define === 'function' && define.amd ? define('@himnark/notifications', ['exports', '@angular/core', '@angular/common', '@angular/material/button', '@angular/material/badge', '@angular/material/menu', '@angular/material/icon', '@angular/material/divider', '@angular/flex-layout', '@ngx-translate/core', '@angular/router', 'moment', '@angular/common/http', 'rxjs/operators', 'rxjs', 'socket.io-client'], factory) :
    (global = global || self, factory((global.himnark = global.himnark || {}, global.himnark.notifications = {}), global.ng.core, global.ng.common, global.ng.material.button, global.ng.material.badge, global.ng.material.menu, global.ng.material.icon, global.ng.material.divider, global.ng['flex-layout'], global.core$1, global.ng.router, global.moment_, global.ng.common.http, global.rxjs.operators, global.rxjs, global.io));
}(this, (function (exports, core, common, button, badge, menu, icon, divider, flexLayout, core$1, router, moment_, http, operators, rxjs, io) { 'use strict';

    io = io && io.hasOwnProperty('default') ? io['default'] : io;

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @record
     */
    function NotificationsConfig() { }
    if (false) {
        /** @type {?} */
        NotificationsConfig.prototype.socketAPI;
        /** @type {?} */
        NotificationsConfig.prototype.notificationAPI;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var NOTIFICATIONS_CONFIG = new core.InjectionToken('NOTIFICATIONS_CONFIG');

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var NotificationsService = /** @class */ (function () {
        function NotificationsService(http, notificationsConfig) {
            this.http = http;
            this.notificationsConfig = notificationsConfig;
        }
        /**
         * @return {?}
         */
        NotificationsService.prototype.getNotifications = /**
         * @return {?}
         */
        function () {
            return this.http.get(this.notificationsConfig.notificationAPI + '/api/user/notifications');
        };
        /**
         * @param {?} data
         * @return {?}
         */
        NotificationsService.prototype.updateNotifications = /**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            return this.http.put(this.notificationsConfig.notificationAPI + '/api/user/notifications', data).pipe(operators.map((/**
             * @param {?} res
             * @return {?}
             */
            function (res) { return res; })));
        };
        /**
         * @param {?} himnarkID
         * @param {?} requestID
         * @param {?} status
         * @param {?} request
         * @return {?}
         */
        NotificationsService.prototype.changeRequestStatus = /**
         * @param {?} himnarkID
         * @param {?} requestID
         * @param {?} status
         * @param {?} request
         * @return {?}
         */
        function (himnarkID, requestID, status, request) {
            /** @type {?} */
            var data = {
                payload: {
                    status: status,
                    type: request.type,
                    details: request.details
                },
                action: 'statusChange'
            };
            return this.http.put(this.notificationsConfig.notificationAPI + '/api/requests/' + requestID, data, {
                headers: new http.HttpHeaders({ himnark: himnarkID })
            }).pipe(operators.map((/**
             * @param {?} res
             * @return {?}
             */
            function (res) { return res; })));
        };
        NotificationsService.decorators = [
            { type: core.Injectable, args: [{
                        providedIn: 'root'
                    },] }
        ];
        /** @nocollapse */
        NotificationsService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [NOTIFICATIONS_CONFIG,] }] }
        ]; };
        /** @nocollapse */ NotificationsService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function NotificationsService_Factory() { return new NotificationsService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(NOTIFICATIONS_CONFIG)); }, token: NotificationsService, providedIn: "root" });
        return NotificationsService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        NotificationsService.prototype.http;
        /**
         * @type {?}
         * @private
         */
        NotificationsService.prototype.notificationsConfig;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SocketService = /** @class */ (function () {
        function SocketService(notificationsConfig) {
            this.notificationsConfig = notificationsConfig;
        }
        // // NOTE it will be nice to create connection in constructor
        // // NOTE it will be nice to create connection in constructor
        /**
         * @param {?} auth0ID
         * @return {?}
         */
        SocketService.prototype.createConnection = 
        // // NOTE it will be nice to create connection in constructor
        /**
         * @param {?} auth0ID
         * @return {?}
         */
        function (auth0ID) {
            this.socket = io(this.notificationsConfig.socketAPI, {
                transports: ['websocket'],
                query: { auth0ID: auth0ID }
            });
        };
        // HANDLER
        // HANDLER
        /**
         * @return {?}
         */
        SocketService.prototype.onStatusUpdate = 
        // HANDLER
        /**
         * @return {?}
         */
        function () {
            var _this = this;
            return rxjs.Observable.create((/**
             * @param {?} observer
             * @return {?}
             */
            function (observer) {
                _this.socket.on('progressUpdate', (/**
                 * @param {?} msg
                 * @return {?}
                 */
                function (msg) {
                    observer.next(msg);
                }));
            }));
        };
        /**
         * @return {?}
         */
        SocketService.prototype.onEventAdded = /**
         * @return {?}
         */
        function () {
            var _this = this;
            return rxjs.Observable.create((/**
             * @param {?} observer
             * @return {?}
             */
            function (observer) {
                _this.socket.on('eventAdded', (/**
                 * @param {?} msg
                 * @return {?}
                 */
                function (msg) {
                    observer.next(msg);
                }));
            }));
        };
        /**
         * @return {?}
         */
        SocketService.prototype.onNewNotification = /**
         * @return {?}
         */
        function () {
            var _this = this;
            return rxjs.Observable.create((/**
             * @param {?} observer
             * @return {?}
             */
            function (observer) {
                _this.socket.on('newNotification', (/**
                 * @param {?} msg
                 * @return {?}
                 */
                function (msg) {
                    observer.next(JSON.parse(msg));
                }));
            }));
        };
        /**
         * @param {?} eventData
         * @return {?}
         */
        SocketService.prototype.addEvent = /**
         * @param {?} eventData
         * @return {?}
         */
        function (eventData) {
            this.socket.send('addEvent', eventData);
        };
        SocketService.decorators = [
            { type: core.Injectable }
        ];
        /** @nocollapse */
        SocketService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [NOTIFICATIONS_CONFIG,] }] }
        ]; };
        return SocketService;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        SocketService.prototype.socket;
        /** @type {?} */
        SocketService.prototype.profile;
        /**
         * @type {?}
         * @private
         */
        SocketService.prototype.notificationsConfig;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var moment = moment_;
    var NotificationsComponent = /** @class */ (function () {
        function NotificationsComponent(router, ns, ts, ss) {
            this.router = router;
            this.ns = ns;
            this.ts = ts;
            this.ss = ss;
            this.notifications = [];
            this.badgeNumber = 0;
            this.hideMatBadge = true;
            this.himnarkChanged = new core.EventEmitter();
        }
        /**
         * @return {?}
         */
        NotificationsComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            var _this = this;
            this.ss.createConnection(this.user.sub);
            this.ns.getNotifications().subscribe((/**
             * @param {?} notifications
             * @return {?}
             */
            function (notifications) {
                if (Array.isArray(notifications) && notifications.length) {
                    _this.notifications = notifications;
                    _this.badgeNumber = notifications.filter((/**
                     * @param {?} item
                     * @return {?}
                     */
                    function (item) { return !item.read; })).length;
                    if (_this.badgeNumber !== 0) {
                        _this.hideMatBadge = false;
                    }
                }
            }));
            this.ss.onNewNotification().subscribe((/**
             * @param {?} notification
             * @return {?}
             */
            function (notification) {
                _this.notifications.unshift(notification);
                _this.badgeNumber += 1;
                _this.hideMatBadge = false;
            }));
        };
        /**
         * @param {?} payload
         * @return {?}
         */
        NotificationsComponent.prototype.getObjForTranslation = /**
         * @param {?} payload
         * @return {?}
         */
        function (payload) {
            /** @type {?} */
            var date = moment(payload.eventDate).format('DD-MM-YYYY');
            if (payload.eventType === 'birthday') {
                date = moment(payload.eventDate).format('DD-MM');
            }
            /** @type {?} */
            var person = '';
            if (payload && payload.person && payload.person.personalInfo) {
                if (payload.person.personalInfo.name) {
                    person += payload.person.personalInfo.name;
                }
                if (payload.person.personalInfo.surname) {
                    person += ' ' + payload.person.personalInfo.surname;
                }
            }
            return { person: person, date: date };
        };
        /**
         * @param {?} himnarkID
         * @param {?} requestID
         * @param {?} status
         * @param {?} request
         * @param {?} event
         * @return {?}
         */
        NotificationsComponent.prototype.updateRequestStatus = /**
         * @param {?} himnarkID
         * @param {?} requestID
         * @param {?} status
         * @param {?} request
         * @param {?} event
         * @return {?}
         */
        function (himnarkID, requestID, status, request, event) {
            var _this = this;
            event.stopPropagation();
            this.ns.changeRequestStatus(himnarkID, requestID, status, request).subscribe((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                /** @type {?} */
                var notification = _this.notifications.find((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.requestObject._id === response.id; }));
                if (notification) {
                    notification.requestObject.status = response.status;
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
            }));
        };
        /**
         * @param {?} himnarkId
         * @param {?} payload
         * @return {?}
         */
        NotificationsComponent.prototype.goTo = /**
         * @param {?} himnarkId
         * @param {?} payload
         * @return {?}
         */
        function (himnarkId, payload) {
            // NOTE it is possible to have different paths depends on application
            if (payload) {
                /** @type {?} */
                var type = payload.type;
                /** @type {?} */
                var path = '/' + himnarkId + '/';
                if (type === 'request') {
                    path += 'tasks/';
                    path += payload.requestType + '/';
                    path += payload.requestNumber;
                }
                else if (type === 'event') {
                    path += 'employees/';
                    path += payload.person._id + '/';
                    /** @type {?} */
                    var eventType = payload.eventType;
                    // NOTE known event types 'birthday', 'trialPeriod', 'termination'
                    if (eventType === 'birthday') {
                        path += 'details';
                    }
                    else {
                        path += 'contracts';
                    }
                }
                /** @type {?} */
                var idFromUrl = window.location.pathname.split('/')[1];
                if (himnarkId !== idFromUrl) {
                    this.himnarkChanged.emit(himnarkId);
                }
                this.router.navigateByUrl(path);
            }
        };
        /**
         * @return {?}
         */
        NotificationsComponent.prototype.read = /**
         * @return {?}
         */
        function () {
            var _this = this;
            /** @type {?} */
            var notReadIDs = this.notifications.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return !item.read; })).map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item._id; }));
            if (notReadIDs.length === 0) {
                return;
            }
            /** @type {?} */
            var data = {
                payload: notReadIDs,
                action: 'read'
            };
            this.ns.updateNotifications(data).subscribe((/**
             * @param {?} updatedIDs
             * @return {?}
             */
            function (updatedIDs) {
                /** @type {?} */
                var readCount = 0;
                if (Array.isArray(updatedIDs)) {
                    readCount = updatedIDs.length;
                }
                _this.badgeNumber -= readCount;
                console.log('this.badgeNumbe', _this.badgeNumber, readCount);
                if (_this.badgeNumber === 0) {
                    _this.hideMatBadge = true;
                }
                _this.notifications.forEach((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) {
                    if (notReadIDs.includes(item._id)) {
                        item.read = true;
                    }
                }));
            }));
        };
        NotificationsComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'himnark-notifications',
                        template: "<div class=\"select-app\">\n    <button mat-icon-button [matMenuTriggerFor]=\"appMenu\" (click)=\"read()\">\n        <mat-icon [matBadge]=\"badgeNumber\" matBadgeColor=\"accent\" [matBadgeHidden]=\"hideMatBadge\">\n            notifications\n        </mat-icon>\n    </button>\n    <mat-menu #appMenu=\"matMenu\" class=\"note-menu\">\n        <div class=\"notifications-block\" (click)=\"$event.stopPropagation()\">\n            <div *ngIf=\"!notifications || notifications.length === 0; else notificationList\"\n                fxLayoutAlign=\"center center\" class=\"empty\">\n                <span>{{ \"NOTIFICATIONS.noNotifications\" | translate }}</span>\n            </div>\n            <ng-template #notificationList>\n                <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"notification-list\">\n                    <div *ngFor=\"let item of notifications; let i = index; let l = last\" (click)=\"goTo(item.himnarkID, item.payload)\"\n                        fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"list-item\">\n                        <div *ngIf=\"item.payload && item.payload.type === 'request'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img src=\"{{ item.who?.picture }}\" alt=\"notifier\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ item.who?.name }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.requestType\">\n                                        <ng-template [ngSwitchCase]=\"'annualVacation'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"island\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'timeOff'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"calendar-remove\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'bonus'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"purple\">\n                                                <mat-icon svgIcon=\"gift\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'businessTrip'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-dark\">\n                                                <mat-icon svgIcon=\"bag-carry-on\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'expenseReport'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-light\">\n                                                <mat-icon fxLayoutAlign=\"center center\">receipt</mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span class=\"payload-text\">{{ item.payload.requestTitle }}</span>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"item.payload && item.payload.type === 'event'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img *ngIf=\"item.from === 'himnarkVaspour'\" alt=\"\" src=\"assets/images/vaspour.png\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ \"NOTIFICATIONS.\" + item.who?.name | translate }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.eventType\">\n                                        <ng-template [ngSwitchCase]=\"'birthday'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"red\">cake</mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'termination'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'trialPeriod'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span>\n                                        {{ \"NOTIFICATIONS.\" + item.payload.eventType | translate:getObjForTranslation(item.payload) }}\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                        <mat-divider *ngIf=\"!l\"></mat-divider>\n                    </div>\n                </div>\n            </ng-template>\n        </div>\n    </mat-menu>\n</div>",
                        providers: [SocketService],
                        styles: ["::ng-deep .note-menu{max-width:480px!important}::ng-deep .note-menu .notifications-block{width:440px}::ng-deep .note-menu .notifications-block .empty{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img{height:40px;width:40px;border-radius:50px;overflow:hidden;box-sizing:border-box}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img img{height:100%;width:100%}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body{width:calc(100% - 56px)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .gray-text{color:rgba(0,0,0,.54)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload{background:rgba(0,0,0,.04);border-radius:3px;padding:8px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .orange{color:#fb8c00}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-dark{color:#006064}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-light{color:#009688}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .purple{color:#9575cd}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .red{color:#ef5350}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload-text{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content:hover{cursor:pointer;background-color:#eee}"]
                    }] }
        ];
        /** @nocollapse */
        NotificationsComponent.ctorParameters = function () { return [
            { type: router.Router },
            { type: NotificationsService },
            { type: core$1.TranslateService },
            { type: SocketService }
        ]; };
        NotificationsComponent.propDecorators = {
            himnarkChanged: [{ type: core.Output }],
            user: [{ type: core.Input }],
            himnarks: [{ type: core.Input }]
        };
        return NotificationsComponent;
    }());
    if (false) {
        /** @type {?} */
        NotificationsComponent.prototype.notifications;
        /** @type {?} */
        NotificationsComponent.prototype.badgeNumber;
        /** @type {?} */
        NotificationsComponent.prototype.hideMatBadge;
        /** @type {?} */
        NotificationsComponent.prototype.himnarkChanged;
        /** @type {?} */
        NotificationsComponent.prototype.user;
        /** @type {?} */
        NotificationsComponent.prototype.himnarks;
        /**
         * @type {?}
         * @private
         */
        NotificationsComponent.prototype.router;
        /**
         * @type {?}
         * @private
         */
        NotificationsComponent.prototype.ns;
        /** @type {?} */
        NotificationsComponent.prototype.ts;
        /**
         * @type {?}
         * @private
         */
        NotificationsComponent.prototype.ss;
    }

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var moment$1 = moment_;
    var FormatDatesPipe = /** @class */ (function () {
        function FormatDatesPipe() {
        }
        /**
         * @param {?} dates
         * @param {?} monthFormat
         * @param {?} typeCase
         * @param {?} lang
         * @return {?}
         */
        FormatDatesPipe.prototype.transform = /**
         * @param {?} dates
         * @param {?} monthFormat
         * @param {?} typeCase
         * @param {?} lang
         * @return {?}
         */
        function (dates, monthFormat, typeCase, lang) {
            /** @type {?} */
            var date = '';
            /** @type {?} */
            var firstDay;
            /** @type {?} */
            var lastDay;
            /** @type {?} */
            var localMoment = moment$1;
            localMoment.locale(lang);
            dates = dates.filter((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item; }));
            if (dates && dates.length) {
                if (localMoment(dates[0], 'DD/MM/YYYY').isValid()) {
                    firstDay = localMoment(dates[0], 'DD/MM/YYYY');
                    lastDay = localMoment(dates[dates.length - 1], 'DD/MM/YYYY');
                }
                else {
                    firstDay = localMoment(dates[0]);
                    lastDay = localMoment(dates[dates.length - 1]);
                }
                if (dates.length === 1) {
                    /** @type {?} */
                    var format = 'DD';
                    if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                        format = format + ', YYYY';
                    }
                    /** @type {?} */
                    var month = firstDay.format(monthFormat);
                    if (typeCase === 'lowercase') {
                        month = month.toLowerCase();
                    }
                    else if (typeCase === 'uppercase') {
                        month = month.toUpperCase();
                    }
                    else {
                        month = month.charAt(0).toUpperCase() + month.slice(1);
                    }
                    date = month + ' ' + firstDay.format(format);
                }
                else {
                    /** @type {?} */
                    var firstMonth = firstDay.format(monthFormat);
                    /** @type {?} */
                    var lastMonth = lastDay.format(monthFormat);
                    if (typeCase === 'lowercase') {
                        firstMonth = firstMonth.toLowerCase();
                        lastMonth = lastMonth.toLowerCase();
                    }
                    else if (typeCase === 'uppercase') {
                        firstMonth = firstMonth.toUpperCase();
                        lastMonth = lastMonth.toUpperCase();
                    }
                    else {
                        firstMonth = firstMonth.charAt(0).toUpperCase() + firstMonth.slice(1);
                        lastMonth = lastMonth.charAt(0).toUpperCase() + lastMonth.slice(1);
                    }
                    if (firstDay.format('YYYY') !== lastDay.format('YYYY')) {
                        /** @type {?} */
                        var format = 'DD';
                        /** @type {?} */
                        var formatLast = 'DD';
                        if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                            format = format + ', YYYY';
                        }
                        if (lastDay.format('YYYY') !== localMoment().format('YYYY')) {
                            formatLast = formatLast + ', YYYY';
                        }
                        date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                    }
                    else if (firstDay.format('YYYY') === lastDay.format('YYYY')) {
                        /** @type {?} */
                        var format = 'DD';
                        /** @type {?} */
                        var formatLast = 'DD';
                        date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                        if (firstDay.format(monthFormat) === lastDay.format(monthFormat)) {
                            format = 'DD';
                            formatLast = 'DD';
                            if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                                formatLast = formatLast + ', YYYY';
                            }
                            date = firstMonth + ' ' + firstDay.format(format) + '-' + lastDay.format(formatLast);
                        }
                    }
                }
            }
            // NOTE Seems that this is not the great way to solve an issue related to moment local
            // anyway, so far so good https://momentjs.com/docs/#/i18n/instance-locale/
            localMoment.locale('en');
            return date;
        };
        FormatDatesPipe.decorators = [
            { type: core.Pipe, args: [{
                        name: 'formatDates'
                    },] }
        ];
        /** @nocollapse */
        FormatDatesPipe.ctorParameters = function () { return []; };
        return FormatDatesPipe;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    // REVIEW is this required?
    var   
    // REVIEW is this required?
    Loader = /** @class */ (function () {
        function Loader() {
            this.translations = new rxjs.Subject();
            this.$translations = this.translations.asObservable();
        }
        /**
         * @param {?} lang
         * @return {?}
         */
        Loader.prototype.getTranslation = /**
         * @param {?} lang
         * @return {?}
         */
        function (lang) {
            return this.$translations;
        };
        return Loader;
    }());
    if (false) {
        /**
         * @type {?}
         * @private
         */
        Loader.prototype.translations;
        /** @type {?} */
        Loader.prototype.$translations;
    }
    /**
     * @return {?}
     */
    function LoaderFactory() {
        return new Loader();
    }
    // @dynamic
    var NotificationsModule = /** @class */ (function () {
        function NotificationsModule() {
        }
        NotificationsModule.decorators = [
            { type: core.NgModule, args: [{
                        imports: [common.CommonModule,
                            badge.MatBadgeModule,
                            button.MatButtonModule,
                            menu.MatMenuModule,
                            icon.MatIconModule,
                            divider.MatDividerModule,
                            flexLayout.FlexLayoutModule,
                            core$1.TranslateModule.forChild({
                                loader: {
                                    provide: core$1.TranslateLoader,
                                    // REVIEW  is this required?
                                    useFactory: LoaderFactory
                                }
                            })],
                        declarations: [NotificationsComponent,
                            FormatDatesPipe],
                        exports: [NotificationsComponent,
                            FormatDatesPipe],
                        providers: [
                            NotificationsService,
                            SocketService,
                            {
                                provide: NOTIFICATIONS_CONFIG,
                                useFactory: ConfigFactory
                            }
                        ]
                    },] }
        ];
        return NotificationsModule;
    }());
    if (false) {
        /** @type {?} */
        NotificationsModule.config;
    }
    /**
     * @return {?}
     */
    function ConfigFactory() {
        return NotificationsModule.config;
    }

    exports.ConfigFactory = ConfigFactory;
    exports.Loader = Loader;
    exports.LoaderFactory = LoaderFactory;
    exports.NOTIFICATIONS_CONFIG = NOTIFICATIONS_CONFIG;
    exports.NotificationsComponent = NotificationsComponent;
    exports.NotificationsModule = NotificationsModule;
    exports.ɵa = SocketService;
    exports.ɵb = NotificationsService;
    exports.ɵc = FormatDatesPipe;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=himnark-notifications.umd.js.map
