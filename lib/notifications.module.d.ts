import { TranslateLoader } from '@ngx-translate/core';
import { NotificationsConfig } from './models/config';
export declare class Loader implements TranslateLoader {
    private translations;
    $translations: import("rxjs").Observable<{}>;
    getTranslation(lang: string): import("rxjs").Observable<{}>;
}
export declare function LoaderFactory(): Loader;
export declare class NotificationsModule {
    static config: NotificationsConfig;
}
export declare function ConfigFactory(): NotificationsConfig;
