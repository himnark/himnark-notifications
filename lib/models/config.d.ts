export interface NotificationsConfig {
    socketAPI: string;
    notificationAPI: string;
}
