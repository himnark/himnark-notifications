import { OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NotificationsService } from '../services/notifications.service';
import { SocketService } from '../services/socket.service';
export declare class NotificationsComponent implements OnInit {
    private router;
    private ns;
    ts: TranslateService;
    private ss;
    notifications: any[];
    badgeNumber: number;
    hideMatBadge: boolean;
    himnarkChanged: EventEmitter<{}>;
    user: any;
    himnarks: [];
    constructor(router: Router, ns: NotificationsService, ts: TranslateService, ss: SocketService);
    ngOnInit(): void;
    getObjForTranslation(payload: any): {
        person: string;
        date: string;
    };
    updateRequestStatus(himnarkID: string, requestID: string, status: string, request: any, event: any): void;
    goTo(himnarkId: string, payload: any): void;
    read(): void;
}
