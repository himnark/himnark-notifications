import { NotificationsConfig } from '../models/config';
export declare class SocketService {
    private notificationsConfig;
    private socket;
    profile: any;
    constructor(notificationsConfig: NotificationsConfig);
    createConnection(auth0ID: string): void;
    onStatusUpdate(): any;
    onEventAdded(): any;
    onNewNotification(): any;
    addEvent(eventData: any): void;
}
