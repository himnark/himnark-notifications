import { HttpClient } from '@angular/common/http';
import { NotificationsConfig } from '../models/config';
export declare class NotificationsService {
    private http;
    private notificationsConfig;
    constructor(http: HttpClient, notificationsConfig: NotificationsConfig);
    getNotifications(): any;
    updateNotifications(data: any): import("rxjs").Observable<Object>;
    changeRequestStatus(himnarkID: string, requestID: string, status: string, request: any): import("rxjs").Observable<Object>;
}
