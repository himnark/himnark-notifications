import { PipeTransform } from '@angular/core';
export declare class FormatDatesPipe implements PipeTransform {
    constructor();
    transform(dates: Array<any>, monthFormat: string, typeCase: string, lang: string): string;
}
