import { InjectionToken, Injectable, Inject, ɵɵdefineInjectable, ɵɵinject, EventEmitter, Component, Output, Input, Pipe, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { Router } from '@angular/router';
import * as moment_ from 'moment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import io from 'socket.io-client';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
function NotificationsConfig() { }
if (false) {
    /** @type {?} */
    NotificationsConfig.prototype.socketAPI;
    /** @type {?} */
    NotificationsConfig.prototype.notificationAPI;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const NOTIFICATIONS_CONFIG = new InjectionToken('NOTIFICATIONS_CONFIG');

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class NotificationsService {
    /**
     * @param {?} http
     * @param {?} notificationsConfig
     */
    constructor(http, notificationsConfig) {
        this.http = http;
        this.notificationsConfig = notificationsConfig;
    }
    /**
     * @return {?}
     */
    getNotifications() {
        return this.http.get(this.notificationsConfig.notificationAPI + '/api/user/notifications');
    }
    /**
     * @param {?} data
     * @return {?}
     */
    updateNotifications(data) {
        return this.http.put(this.notificationsConfig.notificationAPI + '/api/user/notifications', data).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        res => res)));
    }
    /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @return {?}
     */
    changeRequestStatus(himnarkID, requestID, status, request) {
        /** @type {?} */
        const data = {
            payload: {
                status,
                type: request.type,
                details: request.details
            },
            action: 'statusChange'
        };
        return this.http.put(this.notificationsConfig.notificationAPI + '/api/requests/' + requestID, data, {
            headers: new HttpHeaders({ himnark: himnarkID })
        }).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        res => res)));
    }
}
NotificationsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NotificationsService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [NOTIFICATIONS_CONFIG,] }] }
];
/** @nocollapse */ NotificationsService.ngInjectableDef = ɵɵdefineInjectable({ factory: function NotificationsService_Factory() { return new NotificationsService(ɵɵinject(HttpClient), ɵɵinject(NOTIFICATIONS_CONFIG)); }, token: NotificationsService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.http;
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.notificationsConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SocketService {
    /**
     * @param {?} notificationsConfig
     */
    constructor(notificationsConfig) {
        this.notificationsConfig = notificationsConfig;
    }
    // // NOTE it will be nice to create connection in constructor
    /**
     * @param {?} auth0ID
     * @return {?}
     */
    createConnection(auth0ID) {
        this.socket = io(this.notificationsConfig.socketAPI, {
            transports: ['websocket'],
            query: { auth0ID }
        });
    }
    // HANDLER
    /**
     * @return {?}
     */
    onStatusUpdate() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('progressUpdate', (/**
             * @param {?} msg
             * @return {?}
             */
            msg => {
                observer.next(msg);
            }));
        }));
    }
    /**
     * @return {?}
     */
    onEventAdded() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('eventAdded', (/**
             * @param {?} msg
             * @return {?}
             */
            msg => {
                observer.next(msg);
            }));
        }));
    }
    /**
     * @return {?}
     */
    onNewNotification() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('newNotification', (/**
             * @param {?} msg
             * @return {?}
             */
            (msg) => {
                observer.next(JSON.parse(msg));
            }));
        }));
    }
    /**
     * @param {?} eventData
     * @return {?}
     */
    addEvent(eventData) {
        this.socket.send('addEvent', eventData);
    }
}
SocketService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SocketService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [NOTIFICATIONS_CONFIG,] }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.socket;
    /** @type {?} */
    SocketService.prototype.profile;
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.notificationsConfig;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const moment = moment_;
class NotificationsComponent {
    /**
     * @param {?} router
     * @param {?} ns
     * @param {?} ts
     * @param {?} ss
     */
    constructor(router, ns, ts, ss) {
        this.router = router;
        this.ns = ns;
        this.ts = ts;
        this.ss = ss;
        this.notifications = [];
        this.badgeNumber = 0;
        this.hideMatBadge = true;
        this.himnarkChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.ss.createConnection(this.user.sub);
        this.ns.getNotifications().subscribe((/**
         * @param {?} notifications
         * @return {?}
         */
        (notifications) => {
            if (Array.isArray(notifications) && notifications.length) {
                this.notifications = notifications;
                this.badgeNumber = notifications.filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => !item.read)).length;
                if (this.badgeNumber !== 0) {
                    this.hideMatBadge = false;
                }
            }
        }));
        this.ss.onNewNotification().subscribe((/**
         * @param {?} notification
         * @return {?}
         */
        notification => {
            this.notifications.unshift(notification);
            this.badgeNumber += 1;
            this.hideMatBadge = false;
        }));
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    getObjForTranslation(payload) {
        /** @type {?} */
        let date = moment(payload.eventDate).format('DD-MM-YYYY');
        if (payload.eventType === 'birthday') {
            date = moment(payload.eventDate).format('DD-MM');
        }
        /** @type {?} */
        let person = '';
        if (payload && payload.person && payload.person.personalInfo) {
            if (payload.person.personalInfo.name) {
                person += payload.person.personalInfo.name;
            }
            if (payload.person.personalInfo.surname) {
                person += ' ' + payload.person.personalInfo.surname;
            }
        }
        return { person, date };
    }
    /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @param {?} event
     * @return {?}
     */
    updateRequestStatus(himnarkID, requestID, status, request, event) {
        event.stopPropagation();
        this.ns.changeRequestStatus(himnarkID, requestID, status, request).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const notification = this.notifications.find((/**
             * @param {?} item
             * @return {?}
             */
            item => item.requestObject._id === response.id));
            if (notification) {
                notification.requestObject.status = response.status;
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
        }));
    }
    /**
     * @param {?} himnarkId
     * @param {?} payload
     * @return {?}
     */
    goTo(himnarkId, payload) {
        // NOTE it is possible to have different paths depends on application
        if (payload) {
            /** @type {?} */
            const type = payload.type;
            /** @type {?} */
            let path = '/' + himnarkId + '/';
            if (type === 'request') {
                path += 'tasks/';
                path += payload.requestType + '/';
                path += payload.requestNumber;
            }
            else if (type === 'event') {
                path += 'employees/';
                path += payload.person._id + '/';
                /** @type {?} */
                const eventType = payload.eventType;
                // NOTE known event types 'birthday', 'trialPeriod', 'termination'
                if (eventType === 'birthday') {
                    path += 'details';
                }
                else {
                    path += 'contracts';
                }
            }
            /** @type {?} */
            const idFromUrl = window.location.pathname.split('/')[1];
            if (himnarkId !== idFromUrl) {
                this.himnarkChanged.emit(himnarkId);
            }
            this.router.navigateByUrl(path);
        }
    }
    /**
     * @return {?}
     */
    read() {
        /** @type {?} */
        const notReadIDs = this.notifications.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.read)).map((/**
         * @param {?} item
         * @return {?}
         */
        item => item._id));
        if (notReadIDs.length === 0) {
            return;
        }
        /** @type {?} */
        const data = {
            payload: notReadIDs,
            action: 'read'
        };
        this.ns.updateNotifications(data).subscribe((/**
         * @param {?} updatedIDs
         * @return {?}
         */
        (updatedIDs) => {
            /** @type {?} */
            let readCount = 0;
            if (Array.isArray(updatedIDs)) {
                readCount = updatedIDs.length;
            }
            this.badgeNumber -= readCount;
            console.log('this.badgeNumbe', this.badgeNumber, readCount);
            if (this.badgeNumber === 0) {
                this.hideMatBadge = true;
            }
            this.notifications.forEach((/**
             * @param {?} item
             * @return {?}
             */
            item => {
                if (notReadIDs.includes(item._id)) {
                    item.read = true;
                }
            }));
        }));
    }
}
NotificationsComponent.decorators = [
    { type: Component, args: [{
                selector: 'himnark-notifications',
                template: "<div class=\"select-app\">\n    <button mat-icon-button [matMenuTriggerFor]=\"appMenu\" (click)=\"read()\">\n        <mat-icon [matBadge]=\"badgeNumber\" matBadgeColor=\"accent\" [matBadgeHidden]=\"hideMatBadge\">\n            notifications\n        </mat-icon>\n    </button>\n    <mat-menu #appMenu=\"matMenu\" class=\"note-menu\">\n        <div class=\"notifications-block\" (click)=\"$event.stopPropagation()\">\n            <div *ngIf=\"!notifications || notifications.length === 0; else notificationList\"\n                fxLayoutAlign=\"center center\" class=\"empty\">\n                <span>{{ \"NOTIFICATIONS.noNotifications\" | translate }}</span>\n            </div>\n            <ng-template #notificationList>\n                <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"notification-list\">\n                    <div *ngFor=\"let item of notifications; let i = index; let l = last\" (click)=\"goTo(item.himnarkID, item.payload)\"\n                        fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"list-item\">\n                        <div *ngIf=\"item.payload && item.payload.type === 'request'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img src=\"{{ item.who?.picture }}\" alt=\"notifier\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ item.who?.name }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.requestType\">\n                                        <ng-template [ngSwitchCase]=\"'annualVacation'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"island\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'timeOff'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"calendar-remove\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'bonus'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"purple\">\n                                                <mat-icon svgIcon=\"gift\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'businessTrip'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-dark\">\n                                                <mat-icon svgIcon=\"bag-carry-on\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'expenseReport'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-light\">\n                                                <mat-icon fxLayoutAlign=\"center center\">receipt</mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span class=\"payload-text\">{{ item.payload.requestTitle }}</span>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"item.payload && item.payload.type === 'event'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img *ngIf=\"item.from === 'himnarkVaspour'\" alt=\"\" src=\"assets/images/vaspour.png\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ \"NOTIFICATIONS.\" + item.who?.name | translate }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.eventType\">\n                                        <ng-template [ngSwitchCase]=\"'birthday'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"red\">cake</mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'termination'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'trialPeriod'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span>\n                                        {{ \"NOTIFICATIONS.\" + item.payload.eventType | translate:getObjForTranslation(item.payload) }}\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                        <mat-divider *ngIf=\"!l\"></mat-divider>\n                    </div>\n                </div>\n            </ng-template>\n        </div>\n    </mat-menu>\n</div>",
                providers: [SocketService],
                styles: ["::ng-deep .note-menu{max-width:480px!important}::ng-deep .note-menu .notifications-block{width:440px}::ng-deep .note-menu .notifications-block .empty{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img{height:40px;width:40px;border-radius:50px;overflow:hidden;box-sizing:border-box}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img img{height:100%;width:100%}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body{width:calc(100% - 56px)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .gray-text{color:rgba(0,0,0,.54)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload{background:rgba(0,0,0,.04);border-radius:3px;padding:8px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .orange{color:#fb8c00}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-dark{color:#006064}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-light{color:#009688}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .purple{color:#9575cd}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .red{color:#ef5350}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload-text{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content:hover{cursor:pointer;background-color:#eee}"]
            }] }
];
/** @nocollapse */
NotificationsComponent.ctorParameters = () => [
    { type: Router },
    { type: NotificationsService },
    { type: TranslateService },
    { type: SocketService }
];
NotificationsComponent.propDecorators = {
    himnarkChanged: [{ type: Output }],
    user: [{ type: Input }],
    himnarks: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificationsComponent.prototype.notifications;
    /** @type {?} */
    NotificationsComponent.prototype.badgeNumber;
    /** @type {?} */
    NotificationsComponent.prototype.hideMatBadge;
    /** @type {?} */
    NotificationsComponent.prototype.himnarkChanged;
    /** @type {?} */
    NotificationsComponent.prototype.user;
    /** @type {?} */
    NotificationsComponent.prototype.himnarks;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ns;
    /** @type {?} */
    NotificationsComponent.prototype.ts;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ss;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const moment$1 = moment_;
class FormatDatesPipe {
    constructor() {
    }
    /**
     * @param {?} dates
     * @param {?} monthFormat
     * @param {?} typeCase
     * @param {?} lang
     * @return {?}
     */
    transform(dates, monthFormat, typeCase, lang) {
        /** @type {?} */
        let date = '';
        /** @type {?} */
        let firstDay;
        /** @type {?} */
        let lastDay;
        /** @type {?} */
        const localMoment = moment$1;
        localMoment.locale(lang);
        dates = dates.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => item));
        if (dates && dates.length) {
            if (localMoment(dates[0], 'DD/MM/YYYY').isValid()) {
                firstDay = localMoment(dates[0], 'DD/MM/YYYY');
                lastDay = localMoment(dates[dates.length - 1], 'DD/MM/YYYY');
            }
            else {
                firstDay = localMoment(dates[0]);
                lastDay = localMoment(dates[dates.length - 1]);
            }
            if (dates.length === 1) {
                /** @type {?} */
                let format = 'DD';
                if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                    format = format + ', YYYY';
                }
                /** @type {?} */
                let month = firstDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    month = month.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    month = month.toUpperCase();
                }
                else {
                    month = month.charAt(0).toUpperCase() + month.slice(1);
                }
                date = month + ' ' + firstDay.format(format);
            }
            else {
                /** @type {?} */
                let firstMonth = firstDay.format(monthFormat);
                /** @type {?} */
                let lastMonth = lastDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    firstMonth = firstMonth.toLowerCase();
                    lastMonth = lastMonth.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    firstMonth = firstMonth.toUpperCase();
                    lastMonth = lastMonth.toUpperCase();
                }
                else {
                    firstMonth = firstMonth.charAt(0).toUpperCase() + firstMonth.slice(1);
                    lastMonth = lastMonth.charAt(0).toUpperCase() + lastMonth.slice(1);
                }
                if (firstDay.format('YYYY') !== lastDay.format('YYYY')) {
                    /** @type {?} */
                    let format = 'DD';
                    /** @type {?} */
                    let formatLast = 'DD';
                    if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                        format = format + ', YYYY';
                    }
                    if (lastDay.format('YYYY') !== localMoment().format('YYYY')) {
                        formatLast = formatLast + ', YYYY';
                    }
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                }
                else if (firstDay.format('YYYY') === lastDay.format('YYYY')) {
                    /** @type {?} */
                    let format = 'DD';
                    /** @type {?} */
                    let formatLast = 'DD';
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                    if (firstDay.format(monthFormat) === lastDay.format(monthFormat)) {
                        format = 'DD';
                        formatLast = 'DD';
                        if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                            formatLast = formatLast + ', YYYY';
                        }
                        date = firstMonth + ' ' + firstDay.format(format) + '-' + lastDay.format(formatLast);
                    }
                }
            }
        }
        // NOTE Seems that this is not the great way to solve an issue related to moment local
        // anyway, so far so good https://momentjs.com/docs/#/i18n/instance-locale/
        localMoment.locale('en');
        return date;
    }
}
FormatDatesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'formatDates'
            },] }
];
/** @nocollapse */
FormatDatesPipe.ctorParameters = () => [];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
// REVIEW is this required?
class Loader {
    constructor() {
        this.translations = new Subject();
        this.$translations = this.translations.asObservable();
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getTranslation(lang) {
        return this.$translations;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Loader.prototype.translations;
    /** @type {?} */
    Loader.prototype.$translations;
}
/**
 * @return {?}
 */
function LoaderFactory() {
    return new Loader();
}
// @dynamic
class NotificationsModule {
}
NotificationsModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule,
                    MatBadgeModule,
                    MatButtonModule,
                    MatMenuModule,
                    MatIconModule,
                    MatDividerModule,
                    FlexLayoutModule,
                    TranslateModule.forChild({
                        loader: {
                            provide: TranslateLoader,
                            // REVIEW  is this required?
                            useFactory: LoaderFactory
                        }
                    })],
                declarations: [NotificationsComponent,
                    FormatDatesPipe],
                exports: [NotificationsComponent,
                    FormatDatesPipe],
                providers: [
                    NotificationsService,
                    SocketService,
                    {
                        provide: NOTIFICATIONS_CONFIG,
                        useFactory: ConfigFactory
                    }
                ]
            },] }
];
if (false) {
    /** @type {?} */
    NotificationsModule.config;
}
/**
 * @return {?}
 */
function ConfigFactory() {
    return NotificationsModule.config;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ConfigFactory, Loader, LoaderFactory, NOTIFICATIONS_CONFIG, NotificationsComponent, NotificationsModule, SocketService as ɵa, NotificationsService as ɵb, FormatDatesPipe as ɵc };
//# sourceMappingURL=himnark-notifications.js.map
