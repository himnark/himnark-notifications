/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Pipe } from '@angular/core';
import * as moment_ from 'moment';
/** @type {?} */
const moment = moment_;
export class FormatDatesPipe {
    constructor() {
    }
    /**
     * @param {?} dates
     * @param {?} monthFormat
     * @param {?} typeCase
     * @param {?} lang
     * @return {?}
     */
    transform(dates, monthFormat, typeCase, lang) {
        /** @type {?} */
        let date = '';
        /** @type {?} */
        let firstDay;
        /** @type {?} */
        let lastDay;
        /** @type {?} */
        const localMoment = moment;
        localMoment.locale(lang);
        dates = dates.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => item));
        if (dates && dates.length) {
            if (localMoment(dates[0], 'DD/MM/YYYY').isValid()) {
                firstDay = localMoment(dates[0], 'DD/MM/YYYY');
                lastDay = localMoment(dates[dates.length - 1], 'DD/MM/YYYY');
            }
            else {
                firstDay = localMoment(dates[0]);
                lastDay = localMoment(dates[dates.length - 1]);
            }
            if (dates.length === 1) {
                /** @type {?} */
                let format = 'DD';
                if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                    format = format + ', YYYY';
                }
                /** @type {?} */
                let month = firstDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    month = month.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    month = month.toUpperCase();
                }
                else {
                    month = month.charAt(0).toUpperCase() + month.slice(1);
                }
                date = month + ' ' + firstDay.format(format);
            }
            else {
                /** @type {?} */
                let firstMonth = firstDay.format(monthFormat);
                /** @type {?} */
                let lastMonth = lastDay.format(monthFormat);
                if (typeCase === 'lowercase') {
                    firstMonth = firstMonth.toLowerCase();
                    lastMonth = lastMonth.toLowerCase();
                }
                else if (typeCase === 'uppercase') {
                    firstMonth = firstMonth.toUpperCase();
                    lastMonth = lastMonth.toUpperCase();
                }
                else {
                    firstMonth = firstMonth.charAt(0).toUpperCase() + firstMonth.slice(1);
                    lastMonth = lastMonth.charAt(0).toUpperCase() + lastMonth.slice(1);
                }
                if (firstDay.format('YYYY') !== lastDay.format('YYYY')) {
                    /** @type {?} */
                    let format = 'DD';
                    /** @type {?} */
                    let formatLast = 'DD';
                    if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                        format = format + ', YYYY';
                    }
                    if (lastDay.format('YYYY') !== localMoment().format('YYYY')) {
                        formatLast = formatLast + ', YYYY';
                    }
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                }
                else if (firstDay.format('YYYY') === lastDay.format('YYYY')) {
                    /** @type {?} */
                    let format = 'DD';
                    /** @type {?} */
                    let formatLast = 'DD';
                    date = firstMonth + ' ' + firstDay.format(format) + '-' + lastMonth + ' ' + lastDay.format(formatLast);
                    if (firstDay.format(monthFormat) === lastDay.format(monthFormat)) {
                        format = 'DD';
                        formatLast = 'DD';
                        if (firstDay.format('YYYY') !== localMoment().format('YYYY')) {
                            formatLast = formatLast + ', YYYY';
                        }
                        date = firstMonth + ' ' + firstDay.format(format) + '-' + lastDay.format(formatLast);
                    }
                }
            }
        }
        // NOTE Seems that this is not the great way to solve an issue related to moment local
        // anyway, so far so good https://momentjs.com/docs/#/i18n/instance-locale/
        localMoment.locale('en');
        return date;
    }
}
FormatDatesPipe.decorators = [
    { type: Pipe, args: [{
                name: 'formatDates'
            },] }
];
/** @nocollapse */
FormatDatesPipe.ctorParameters = () => [];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LWRhdGVzLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL3BpcGVzL2Zvcm1hdC1kYXRlcy5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQzs7TUFDNUIsTUFBTSxHQUFHLE9BQU87QUFLdEIsTUFBTSxPQUFPLGVBQWU7SUFFeEI7SUFDQSxDQUFDOzs7Ozs7OztJQUVELFNBQVMsQ0FBQyxLQUFpQixFQUFFLFdBQW1CLEVBQUUsUUFBZ0IsRUFBRSxJQUFZOztZQUN4RSxJQUFJLEdBQUcsRUFBRTs7WUFDVCxRQUFROztZQUNSLE9BQU87O2NBQ0wsV0FBVyxHQUFHLE1BQU07UUFDMUIsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN6QixLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU07Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksRUFBQyxDQUFDO1FBQ25DLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDdkIsSUFBSSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLFlBQVksQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUMvQyxRQUFRLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQztnQkFDL0MsT0FBTyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUNoRTtpQkFBTTtnQkFDSCxRQUFRLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxPQUFPLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEQ7WUFDRCxJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFOztvQkFDaEIsTUFBTSxHQUFHLElBQUk7Z0JBQ2pCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQzFELE1BQU0sR0FBRyxNQUFNLEdBQUcsUUFBUSxDQUFDO2lCQUM5Qjs7b0JBQ0csS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO2dCQUN4QyxJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7b0JBQzFCLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQy9CO3FCQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtvQkFDakMsS0FBSyxHQUFHLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDL0I7cUJBQU07b0JBQ0gsS0FBSyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUQ7Z0JBQ0QsSUFBSSxHQUFHLEtBQUssR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNoRDtpQkFBTTs7b0JBQ0MsVUFBVSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDOztvQkFDekMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDO2dCQUMzQyxJQUFJLFFBQVEsS0FBSyxXQUFXLEVBQUU7b0JBQzFCLFVBQVUsR0FBRyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3RDLFNBQVMsR0FBRyxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3ZDO3FCQUFNLElBQUksUUFBUSxLQUFLLFdBQVcsRUFBRTtvQkFDakMsVUFBVSxHQUFHLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDdEMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztpQkFDdkM7cUJBQU07b0JBQ0gsVUFBVSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdEUsU0FBUyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDdEU7Z0JBQ0QsSUFBSSxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7O3dCQUNoRCxNQUFNLEdBQUcsSUFBSTs7d0JBQ2IsVUFBVSxHQUFHLElBQUk7b0JBQ3JCLElBQUksUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUU7d0JBQzFELE1BQU0sR0FBRyxNQUFNLEdBQUcsUUFBUSxDQUFDO3FCQUM5QjtvQkFDRCxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFO3dCQUN6RCxVQUFVLEdBQUcsVUFBVSxHQUFHLFFBQVEsQ0FBQztxQkFDdEM7b0JBQ0QsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUMxRztxQkFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRTs7d0JBQ3ZELE1BQU0sR0FBRyxJQUFJOzt3QkFDYixVQUFVLEdBQUcsSUFBSTtvQkFDckIsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUcsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN2RyxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLEtBQUssT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsRUFBRTt3QkFDOUQsTUFBTSxHQUFHLElBQUksQ0FBQzt3QkFDZCxVQUFVLEdBQUcsSUFBSSxDQUFDO3dCQUNsQixJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFOzRCQUMxRCxVQUFVLEdBQUcsVUFBVSxHQUFHLFFBQVEsQ0FBQzt5QkFDdEM7d0JBQ0QsSUFBSSxHQUFHLFVBQVUsR0FBRyxHQUFHLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDeEY7aUJBQ0o7YUFDSjtTQUNKO1FBQ0Qsc0ZBQXNGO1FBQ3RGLDJFQUEyRTtRQUMzRSxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7OztZQS9FSixJQUFJLFNBQUM7Z0JBQ0YsSUFBSSxFQUFFLGFBQWE7YUFDdEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XG5jb25zdCBtb21lbnQgPSBtb21lbnRfO1xuXG5AUGlwZSh7XG4gICAgbmFtZTogJ2Zvcm1hdERhdGVzJ1xufSlcbmV4cG9ydCBjbGFzcyBGb3JtYXREYXRlc1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgIH1cblxuICAgIHRyYW5zZm9ybShkYXRlczogQXJyYXk8YW55PiwgbW9udGhGb3JtYXQ6IHN0cmluZywgdHlwZUNhc2U6IHN0cmluZywgbGFuZzogc3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgbGV0IGRhdGUgPSAnJztcbiAgICAgICAgbGV0IGZpcnN0RGF5O1xuICAgICAgICBsZXQgbGFzdERheTtcbiAgICAgICAgY29uc3QgbG9jYWxNb21lbnQgPSBtb21lbnQ7XG4gICAgICAgIGxvY2FsTW9tZW50LmxvY2FsZShsYW5nKTtcbiAgICAgICAgZGF0ZXMgPSBkYXRlcy5maWx0ZXIoaXRlbSA9PiBpdGVtKTtcbiAgICAgICAgaWYgKGRhdGVzICYmIGRhdGVzLmxlbmd0aCkge1xuICAgICAgICAgICAgaWYgKGxvY2FsTW9tZW50KGRhdGVzWzBdLCAnREQvTU0vWVlZWScpLmlzVmFsaWQoKSkge1xuICAgICAgICAgICAgICAgIGZpcnN0RGF5ID0gbG9jYWxNb21lbnQoZGF0ZXNbMF0sICdERC9NTS9ZWVlZJyk7XG4gICAgICAgICAgICAgICAgbGFzdERheSA9IGxvY2FsTW9tZW50KGRhdGVzW2RhdGVzLmxlbmd0aCAtIDFdLCAnREQvTU0vWVlZWScpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBmaXJzdERheSA9IGxvY2FsTW9tZW50KGRhdGVzWzBdKTtcbiAgICAgICAgICAgICAgICBsYXN0RGF5ID0gbG9jYWxNb21lbnQoZGF0ZXNbZGF0ZXMubGVuZ3RoIC0gMV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKGRhdGVzLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgICAgICAgIGxldCBmb3JtYXQgPSAnREQnO1xuICAgICAgICAgICAgICAgIGlmIChmaXJzdERheS5mb3JtYXQoJ1lZWVknKSAhPT0gbG9jYWxNb21lbnQoKS5mb3JtYXQoJ1lZWVknKSkge1xuICAgICAgICAgICAgICAgICAgICBmb3JtYXQgPSBmb3JtYXQgKyAnLCBZWVlZJztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgbGV0IG1vbnRoID0gZmlyc3REYXkuZm9ybWF0KG1vbnRoRm9ybWF0KTtcbiAgICAgICAgICAgICAgICBpZiAodHlwZUNhc2UgPT09ICdsb3dlcmNhc2UnKSB7XG4gICAgICAgICAgICAgICAgICAgIG1vbnRoID0gbW9udGgudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVDYXNlID09PSAndXBwZXJjYXNlJykge1xuICAgICAgICAgICAgICAgICAgICBtb250aCA9IG1vbnRoLnRvVXBwZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbW9udGggPSBtb250aC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIG1vbnRoLnNsaWNlKDEpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBkYXRlID0gbW9udGggKyAnICcgKyBmaXJzdERheS5mb3JtYXQoZm9ybWF0KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgbGV0IGZpcnN0TW9udGggPSBmaXJzdERheS5mb3JtYXQobW9udGhGb3JtYXQpO1xuICAgICAgICAgICAgICAgIGxldCBsYXN0TW9udGggPSBsYXN0RGF5LmZvcm1hdChtb250aEZvcm1hdCk7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVDYXNlID09PSAnbG93ZXJjYXNlJykge1xuICAgICAgICAgICAgICAgICAgICBmaXJzdE1vbnRoID0gZmlyc3RNb250aC50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgICAgICAgICBsYXN0TW9udGggPSBsYXN0TW9udGgudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGVDYXNlID09PSAndXBwZXJjYXNlJykge1xuICAgICAgICAgICAgICAgICAgICBmaXJzdE1vbnRoID0gZmlyc3RNb250aC50b1VwcGVyQ2FzZSgpO1xuICAgICAgICAgICAgICAgICAgICBsYXN0TW9udGggPSBsYXN0TW9udGgudG9VcHBlckNhc2UoKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBmaXJzdE1vbnRoID0gZmlyc3RNb250aC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIGZpcnN0TW9udGguc2xpY2UoMSk7XG4gICAgICAgICAgICAgICAgICAgIGxhc3RNb250aCA9IGxhc3RNb250aC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIGxhc3RNb250aC5zbGljZSgxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgaWYgKGZpcnN0RGF5LmZvcm1hdCgnWVlZWScpICE9PSBsYXN0RGF5LmZvcm1hdCgnWVlZWScpKSB7XG4gICAgICAgICAgICAgICAgICAgIGxldCBmb3JtYXQgPSAnREQnO1xuICAgICAgICAgICAgICAgICAgICBsZXQgZm9ybWF0TGFzdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgICAgIGlmIChmaXJzdERheS5mb3JtYXQoJ1lZWVknKSAhPT0gbG9jYWxNb21lbnQoKS5mb3JtYXQoJ1lZWVknKSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0ID0gZm9ybWF0ICsgJywgWVlZWSc7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKGxhc3REYXkuZm9ybWF0KCdZWVlZJykgIT09IGxvY2FsTW9tZW50KCkuZm9ybWF0KCdZWVlZJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdExhc3QgPSBmb3JtYXRMYXN0ICsgJywgWVlZWSc7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgZGF0ZSA9IGZpcnN0TW9udGggKyAnICcgKyBmaXJzdERheS5mb3JtYXQoZm9ybWF0KSArICctJyArIGxhc3RNb250aCArICcgJyArIGxhc3REYXkuZm9ybWF0KGZvcm1hdExhc3QpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoZmlyc3REYXkuZm9ybWF0KCdZWVlZJykgPT09IGxhc3REYXkuZm9ybWF0KCdZWVlZJykpIHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IGZvcm1hdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgICAgIGxldCBmb3JtYXRMYXN0ID0gJ0REJztcbiAgICAgICAgICAgICAgICAgICAgZGF0ZSA9IGZpcnN0TW9udGggKyAnICcgKyBmaXJzdERheS5mb3JtYXQoZm9ybWF0KSArICctJyArIGxhc3RNb250aCArICcgJyArIGxhc3REYXkuZm9ybWF0KGZvcm1hdExhc3QpO1xuICAgICAgICAgICAgICAgICAgICBpZiAoZmlyc3REYXkuZm9ybWF0KG1vbnRoRm9ybWF0KSA9PT0gbGFzdERheS5mb3JtYXQobW9udGhGb3JtYXQpKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXQgPSAnREQnO1xuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0TGFzdCA9ICdERCc7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZmlyc3REYXkuZm9ybWF0KCdZWVlZJykgIT09IGxvY2FsTW9tZW50KCkuZm9ybWF0KCdZWVlZJykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXRMYXN0ID0gZm9ybWF0TGFzdCArICcsIFlZWVknO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZSA9IGZpcnN0TW9udGggKyAnICcgKyBmaXJzdERheS5mb3JtYXQoZm9ybWF0KSArICctJyArIGxhc3REYXkuZm9ybWF0KGZvcm1hdExhc3QpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vIE5PVEUgU2VlbXMgdGhhdCB0aGlzIGlzIG5vdCB0aGUgZ3JlYXQgd2F5IHRvIHNvbHZlIGFuIGlzc3VlIHJlbGF0ZWQgdG8gbW9tZW50IGxvY2FsXG4gICAgICAgIC8vIGFueXdheSwgc28gZmFyIHNvIGdvb2QgaHR0cHM6Ly9tb21lbnRqcy5jb20vZG9jcy8jL2kxOG4vaW5zdGFuY2UtbG9jYWxlL1xuICAgICAgICBsb2NhbE1vbWVudC5sb2NhbGUoJ2VuJyk7XG4gICAgICAgIHJldHVybiBkYXRlO1xuICAgIH1cbn1cbiJdfQ==