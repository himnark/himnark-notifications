/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { NotificationsService } from '../services/notifications.service';
import { SocketService } from '../services/socket.service';
/** @type {?} */
const moment = moment_;
export class NotificationsComponent {
    /**
     * @param {?} router
     * @param {?} ns
     * @param {?} ts
     * @param {?} ss
     */
    constructor(router, ns, ts, ss) {
        this.router = router;
        this.ns = ns;
        this.ts = ts;
        this.ss = ss;
        this.notifications = [];
        this.badgeNumber = 0;
        this.hideMatBadge = true;
        this.himnarkChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.ss.createConnection(this.user.sub);
        this.ns.getNotifications().subscribe((/**
         * @param {?} notifications
         * @return {?}
         */
        (notifications) => {
            if (Array.isArray(notifications) && notifications.length) {
                this.notifications = notifications;
                this.badgeNumber = notifications.filter((/**
                 * @param {?} item
                 * @return {?}
                 */
                item => !item.read)).length;
                if (this.badgeNumber !== 0) {
                    this.hideMatBadge = false;
                }
            }
        }));
        this.ss.onNewNotification().subscribe((/**
         * @param {?} notification
         * @return {?}
         */
        notification => {
            this.notifications.unshift(notification);
            this.badgeNumber += 1;
            this.hideMatBadge = false;
        }));
    }
    /**
     * @param {?} payload
     * @return {?}
     */
    getObjForTranslation(payload) {
        /** @type {?} */
        let date = moment(payload.eventDate).format('DD-MM-YYYY');
        if (payload.eventType === 'birthday') {
            date = moment(payload.eventDate).format('DD-MM');
        }
        /** @type {?} */
        let person = '';
        if (payload && payload.person && payload.person.personalInfo) {
            if (payload.person.personalInfo.name) {
                person += payload.person.personalInfo.name;
            }
            if (payload.person.personalInfo.surname) {
                person += ' ' + payload.person.personalInfo.surname;
            }
        }
        return { person, date };
    }
    /**
     * @param {?} himnarkID
     * @param {?} requestID
     * @param {?} status
     * @param {?} request
     * @param {?} event
     * @return {?}
     */
    updateRequestStatus(himnarkID, requestID, status, request, event) {
        event.stopPropagation();
        this.ns.changeRequestStatus(himnarkID, requestID, status, request).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const notification = this.notifications.find((/**
             * @param {?} item
             * @return {?}
             */
            item => item.requestObject._id === response.id));
            if (notification) {
                notification.requestObject.status = response.status;
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
        }));
    }
    /**
     * @param {?} himnarkId
     * @param {?} payload
     * @return {?}
     */
    goTo(himnarkId, payload) {
        // NOTE it is possible to have different paths depends on application
        if (payload) {
            /** @type {?} */
            const type = payload.type;
            /** @type {?} */
            let path = '/' + himnarkId + '/';
            if (type === 'request') {
                path += 'tasks/';
                path += payload.requestType + '/';
                path += payload.requestNumber;
            }
            else if (type === 'event') {
                path += 'employees/';
                path += payload.person._id + '/';
                /** @type {?} */
                const eventType = payload.eventType;
                // NOTE known event types 'birthday', 'trialPeriod', 'termination'
                if (eventType === 'birthday') {
                    path += 'details';
                }
                else {
                    path += 'contracts';
                }
            }
            /** @type {?} */
            const idFromUrl = window.location.pathname.split('/')[1];
            if (himnarkId !== idFromUrl) {
                this.himnarkChanged.emit(himnarkId);
            }
            this.router.navigateByUrl(path);
        }
    }
    /**
     * @return {?}
     */
    read() {
        /** @type {?} */
        const notReadIDs = this.notifications.filter((/**
         * @param {?} item
         * @return {?}
         */
        item => !item.read)).map((/**
         * @param {?} item
         * @return {?}
         */
        item => item._id));
        if (notReadIDs.length === 0) {
            return;
        }
        /** @type {?} */
        const data = {
            payload: notReadIDs,
            action: 'read'
        };
        this.ns.updateNotifications(data).subscribe((/**
         * @param {?} updatedIDs
         * @return {?}
         */
        (updatedIDs) => {
            /** @type {?} */
            let readCount = 0;
            if (Array.isArray(updatedIDs)) {
                readCount = updatedIDs.length;
            }
            this.badgeNumber -= readCount;
            console.log('this.badgeNumbe', this.badgeNumber, readCount);
            if (this.badgeNumber === 0) {
                this.hideMatBadge = true;
            }
            this.notifications.forEach((/**
             * @param {?} item
             * @return {?}
             */
            item => {
                if (notReadIDs.includes(item._id)) {
                    item.read = true;
                }
            }));
        }));
    }
}
NotificationsComponent.decorators = [
    { type: Component, args: [{
                selector: 'himnark-notifications',
                template: "<div class=\"select-app\">\n    <button mat-icon-button [matMenuTriggerFor]=\"appMenu\" (click)=\"read()\">\n        <mat-icon [matBadge]=\"badgeNumber\" matBadgeColor=\"accent\" [matBadgeHidden]=\"hideMatBadge\">\n            notifications\n        </mat-icon>\n    </button>\n    <mat-menu #appMenu=\"matMenu\" class=\"note-menu\">\n        <div class=\"notifications-block\" (click)=\"$event.stopPropagation()\">\n            <div *ngIf=\"!notifications || notifications.length === 0; else notificationList\"\n                fxLayoutAlign=\"center center\" class=\"empty\">\n                <span>{{ \"NOTIFICATIONS.noNotifications\" | translate }}</span>\n            </div>\n            <ng-template #notificationList>\n                <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"notification-list\">\n                    <div *ngFor=\"let item of notifications; let i = index; let l = last\" (click)=\"goTo(item.himnarkID, item.payload)\"\n                        fxLayout=\"column\" fxLayoutAlign=\"start stretch\" class=\"list-item\">\n                        <div *ngIf=\"item.payload && item.payload.type === 'request'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img src=\"{{ item.who?.picture }}\" alt=\"notifier\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ item.who?.name }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.requestType\">\n                                        <ng-template [ngSwitchCase]=\"'annualVacation'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"island\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'timeOff'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"orange\">\n                                                <mat-icon svgIcon=\"calendar-remove\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'bonus'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"purple\">\n                                                <mat-icon svgIcon=\"gift\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'businessTrip'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-dark\">\n                                                <mat-icon svgIcon=\"bag-carry-on\"></mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'expenseReport'\">\n                                            <div fxLayoutAlign=\"start center\" fxLayoutGap=\"8px\" class=\"green-light\">\n                                                <mat-icon fxLayoutAlign=\"center center\">receipt</mat-icon>\n                                                <span>{{ item.payload.requestNumber }}</span>\n                                            </div>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span class=\"payload-text\">{{ item.payload.requestTitle }}</span>\n                                </div>\n                            </div>\n                        </div>\n                        <div *ngIf=\"item.payload && item.payload.type === 'event'\"\n                            fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"16px\" class=\"item-content\">\n                            <div class=\"avatar-img\">\n                                <img *ngIf=\"item.from === 'himnarkVaspour'\" alt=\"\" src=\"assets/images/vaspour.png\">\n                            </div>\n                            <div fxLayout=\"column\" fxLayoutAlign=\"start stretch\" fxLayoutGap=\"8px\" class=\"content-body\">\n                                <div fxLayout=\"row\" fxLayoutAlign=\"space-between start\" fxLayoutGap=\"16px\">\n                                    <div fxLayout=\"column\">\n                                        <span>\n                                            <b>{{ \"NOTIFICATIONS.\" + item.who?.name | translate }}</b>\n                                            <span>{{ \"NOTIFICATIONS.subjective\" | translate }}&nbsp;</span>\n                                            <span class=\"gray-text\">\n                                                {{ \"NOTIFICATIONS.\" + item.action | translate }}\n                                            </span>\n                                        </span>\n                                    </div>\n                                    <div>\n                                        <span class=\"gray-text\">{{ [item.date] | formatDates: 'MMM': '':\n                                            ts.currentLang === 'hy' ? 'hy-am' : 'en' }}\n                                        </span>\n                                        <!-- <button mat-icon-button [matMenuTriggerFor]=\"itemMenu\">\n                                            <mat-icon class=\"gray-text\">more_vert</mat-icon>\n                                        </button>\n                                        <mat-menu #itemMenu=\"matMenu\">\n                                            <button mat-menu-item disabled>\n                                                <mat-icon class=\"gray-text\">archive</mat-icon>\n                                                <span>{{ \"COMMON.archive\" | translate }}</span>\n                                            </button>\n                                        </mat-menu> -->\n                                    </div>\n                                </div>\n                                <div class=\"payload\" fxLayout=\"row\" fxLayoutAlign=\"start start\" fxLayoutGap=\"8px\">\n                                    <ng-container [ngSwitch]=\"item.payload.eventType\">\n                                        <ng-template [ngSwitchCase]=\"'birthday'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"red\">cake</mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'termination'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                        <ng-template [ngSwitchCase]=\"'trialPeriod'\">\n                                            <mat-icon fxLayoutAlign=\"center center\" class=\"green-dark\">\n                                                description\n                                            </mat-icon>\n                                        </ng-template>\n                                    </ng-container>\n                                    <span>\n                                        {{ \"NOTIFICATIONS.\" + item.payload.eventType | translate:getObjForTranslation(item.payload) }}\n                                    </span>\n                                </div>\n                            </div>\n                        </div>\n                        <mat-divider *ngIf=\"!l\"></mat-divider>\n                    </div>\n                </div>\n            </ng-template>\n        </div>\n    </mat-menu>\n</div>",
                providers: [SocketService],
                styles: ["::ng-deep .note-menu{max-width:480px!important}::ng-deep .note-menu .notifications-block{width:440px}::ng-deep .note-menu .notifications-block .empty{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content{padding:16px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img{height:40px;width:40px;border-radius:50px;overflow:hidden;box-sizing:border-box}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .avatar-img img{height:100%;width:100%}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body{width:calc(100% - 56px)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .gray-text{color:rgba(0,0,0,.54)}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload{background:rgba(0,0,0,.04);border-radius:3px;padding:8px}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .orange{color:#fb8c00}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-dark{color:#006064}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .green-light{color:#009688}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .purple{color:#9575cd}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload .red{color:#ef5350}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content .content-body .payload-text{white-space:nowrap;overflow:hidden;text-overflow:ellipsis}::ng-deep .note-menu .notifications-block .notification-list .list-item .item-content:hover{cursor:pointer;background-color:#eee}"]
            }] }
];
/** @nocollapse */
NotificationsComponent.ctorParameters = () => [
    { type: Router },
    { type: NotificationsService },
    { type: TranslateService },
    { type: SocketService }
];
NotificationsComponent.propDecorators = {
    himnarkChanged: [{ type: Output }],
    user: [{ type: Input }],
    himnarks: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificationsComponent.prototype.notifications;
    /** @type {?} */
    NotificationsComponent.prototype.badgeNumber;
    /** @type {?} */
    NotificationsComponent.prototype.hideMatBadge;
    /** @type {?} */
    NotificationsComponent.prototype.himnarkChanged;
    /** @type {?} */
    NotificationsComponent.prototype.user;
    /** @type {?} */
    NotificationsComponent.prototype.himnarks;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ns;
    /** @type {?} */
    NotificationsComponent.prototype.ts;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.ss;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbnMvbm90aWZpY2F0aW9ucy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBRWxDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7TUFDckQsTUFBTSxHQUFHLE9BQU87QUFRdEIsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7OztJQVMvQixZQUNZLE1BQWMsRUFDZCxFQUF3QixFQUN6QixFQUFvQixFQUNuQixFQUFpQjtRQUhqQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsT0FBRSxHQUFGLEVBQUUsQ0FBc0I7UUFDekIsT0FBRSxHQUFGLEVBQUUsQ0FBa0I7UUFDbkIsT0FBRSxHQUFGLEVBQUUsQ0FBZTtRQVg3QixrQkFBYSxHQUFVLEVBQUUsQ0FBQztRQUMxQixnQkFBVyxHQUFHLENBQUMsQ0FBQztRQUNoQixpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNWLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQVMxQyxDQUFDOzs7O0lBRUwsUUFBUTtRQUNKLElBQUksQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsRUFBRSxDQUFDLGdCQUFnQixFQUFFLENBQUMsU0FBUzs7OztRQUFDLENBQUMsYUFBa0IsRUFBRSxFQUFFO1lBQ3hELElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxhQUFhLENBQUMsTUFBTSxFQUFFO2dCQUN0RCxJQUFJLENBQUMsYUFBYSxHQUFHLGFBQWEsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUMsTUFBTTs7OztnQkFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLE1BQU0sQ0FBQztnQkFDbkUsSUFBSSxJQUFJLENBQUMsV0FBVyxLQUFLLENBQUMsRUFBRTtvQkFDeEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7aUJBQzdCO2FBQ0o7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTOzs7O1FBQUMsWUFBWSxDQUFDLEVBQUU7WUFDakQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLFdBQVcsSUFBSSxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDOUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELG9CQUFvQixDQUFDLE9BQVk7O1lBQ3pCLElBQUksR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7UUFDekQsSUFBSSxPQUFPLENBQUMsU0FBUyxLQUFLLFVBQVUsRUFBRTtZQUNsQyxJQUFJLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDcEQ7O1lBQ0csTUFBTSxHQUFHLEVBQUU7UUFDZixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFO1lBQzFELElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFO2dCQUNsQyxNQUFNLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO2FBQzlDO1lBQ0QsSUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUU7Z0JBQ3JDLE1BQU0sSUFBSSxHQUFHLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDO2FBQ3ZEO1NBQ0o7UUFDRCxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7Ozs7OztJQUVELG1CQUFtQixDQUFDLFNBQWlCLEVBQUUsU0FBaUIsRUFBRSxNQUFjLEVBQUUsT0FBWSxFQUFFLEtBQUs7UUFDekYsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxFQUFFLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUMsU0FBUzs7OztRQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7O2tCQUNqRixZQUFZLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJOzs7O1lBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsS0FBSyxRQUFRLENBQUMsRUFBRSxFQUFDO1lBQzVGLElBQUksWUFBWSxFQUFFO2dCQUNkLFlBQVksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUM7YUFDdkQ7UUFDVCxDQUFDOzs7O1FBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRTtRQUNYLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsSUFBSSxDQUFDLFNBQWlCLEVBQUUsT0FBWTtRQUNoQyxxRUFBcUU7UUFDckUsSUFBSSxPQUFPLEVBQUU7O2tCQUNILElBQUksR0FBRyxPQUFPLENBQUMsSUFBSTs7Z0JBQ3JCLElBQUksR0FBRyxHQUFHLEdBQUcsU0FBUyxHQUFHLEdBQUc7WUFDaEMsSUFBSSxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUNwQixJQUFJLElBQUksUUFBUSxDQUFDO2dCQUNqQixJQUFJLElBQUksT0FBTyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7Z0JBQ2xDLElBQUksSUFBSSxPQUFPLENBQUMsYUFBYSxDQUFDO2FBQ2pDO2lCQUFNLElBQUksSUFBSSxLQUFLLE9BQU8sRUFBRTtnQkFDekIsSUFBSSxJQUFJLFlBQVksQ0FBQztnQkFDckIsSUFBSSxJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQzs7c0JBQzNCLFNBQVMsR0FBRyxPQUFPLENBQUMsU0FBUztnQkFDbkMsa0VBQWtFO2dCQUNsRSxJQUFJLFNBQVMsS0FBSyxVQUFVLEVBQUU7b0JBQzFCLElBQUksSUFBSSxTQUFTLENBQUM7aUJBQ3JCO3FCQUFNO29CQUNILElBQUksSUFBSSxXQUFXLENBQUM7aUJBQ3ZCO2FBQ0o7O2tCQUNLLFNBQVMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3hELElBQUksU0FBUyxLQUFLLFNBQVMsRUFBRTtnQkFDekIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7SUFFRCxJQUFJOztjQUNNLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU07Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBQyxDQUFDLEdBQUc7Ozs7UUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUM7UUFDdEYsSUFBSSxVQUFVLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUN6QixPQUFPO1NBQ1Y7O2NBQ0ssSUFBSSxHQUFHO1lBQ1QsT0FBTyxFQUFFLFVBQVU7WUFDbkIsTUFBTSxFQUFFLE1BQU07U0FDakI7UUFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLFVBQWUsRUFBRSxFQUFFOztnQkFDeEQsU0FBUyxHQUFHLENBQUM7WUFDakIsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUMzQixTQUFTLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQzthQUNqQztZQUNELElBQUksQ0FBQyxXQUFXLElBQUksU0FBUyxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztZQUM1RCxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssQ0FBQyxFQUFFO2dCQUN4QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzthQUM1QjtZQUNELElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztZQUFDLElBQUksQ0FBQyxFQUFFO2dCQUM5QixJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO29CQUMvQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztpQkFDcEI7WUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7O1lBekhKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsdUJBQXVCO2dCQUNqQyxzM1VBQTZDO2dCQUU3QyxTQUFTLEVBQUcsQ0FBRSxhQUFhLENBQUU7O2FBQ2hDOzs7O1lBYlEsTUFBTTtZQUlOLG9CQUFvQjtZQUhwQixnQkFBZ0I7WUFJaEIsYUFBYTs7OzZCQWNqQixNQUFNO21CQUNOLEtBQUs7dUJBQ0wsS0FBSzs7OztJQUxOLCtDQUEwQjs7SUFDMUIsNkNBQWdCOztJQUNoQiw4Q0FBb0I7O0lBQ3BCLGdEQUE4Qzs7SUFDOUMsc0NBQW1COztJQUNuQiwwQ0FBc0I7Ozs7O0lBR2xCLHdDQUFzQjs7Ozs7SUFDdEIsb0NBQWdDOztJQUNoQyxvQ0FBMkI7Ozs7O0lBQzNCLG9DQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XG5cbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbm90aWZpY2F0aW9ucy5zZXJ2aWNlJztcbmltcG9ydCB7IFNvY2tldFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zb2NrZXQuc2VydmljZSc7XG5jb25zdCBtb21lbnQgPSBtb21lbnRfO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2hpbW5hcmstbm90aWZpY2F0aW9ucycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL25vdGlmaWNhdGlvbnMuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL25vdGlmaWNhdGlvbnMuY29tcG9uZW50LnNjc3MnXSxcbiAgICBwcm92aWRlcnM6ICBbIFNvY2tldFNlcnZpY2UgXVxufSlcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIG5vdGlmaWNhdGlvbnM6IGFueVtdID0gW107XG4gICAgYmFkZ2VOdW1iZXIgPSAwO1xuICAgIGhpZGVNYXRCYWRnZSA9IHRydWU7XG4gICAgQE91dHB1dCgpIGhpbW5hcmtDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuICAgIEBJbnB1dCgpIHVzZXI6IGFueTtcbiAgICBASW5wdXQoKSBoaW1uYXJrczogW107XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcbiAgICAgICAgcHJpdmF0ZSBuczogTm90aWZpY2F0aW9uc1NlcnZpY2UsXG4gICAgICAgIHB1YmxpYyB0czogVHJhbnNsYXRlU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBzczogU29ja2V0U2VydmljZVxuICAgICkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5zcy5jcmVhdGVDb25uZWN0aW9uKHRoaXMudXNlci5zdWIpO1xuICAgICAgICB0aGlzLm5zLmdldE5vdGlmaWNhdGlvbnMoKS5zdWJzY3JpYmUoKG5vdGlmaWNhdGlvbnM6IGFueSkgPT4ge1xuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkobm90aWZpY2F0aW9ucykgJiYgbm90aWZpY2F0aW9ucy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMgPSBub3RpZmljYXRpb25zO1xuICAgICAgICAgICAgICAgIHRoaXMuYmFkZ2VOdW1iZXIgPSBub3RpZmljYXRpb25zLmZpbHRlcihpdGVtID0+ICFpdGVtLnJlYWQpLmxlbmd0aDtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5iYWRnZU51bWJlciAhPT0gMCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpZGVNYXRCYWRnZSA9IGZhbHNlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuc3Mub25OZXdOb3RpZmljYXRpb24oKS5zdWJzY3JpYmUobm90aWZpY2F0aW9uID0+IHtcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy51bnNoaWZ0KG5vdGlmaWNhdGlvbik7XG4gICAgICAgICAgICB0aGlzLmJhZGdlTnVtYmVyICs9IDE7XG4gICAgICAgICAgICB0aGlzLmhpZGVNYXRCYWRnZSA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBnZXRPYmpGb3JUcmFuc2xhdGlvbihwYXlsb2FkOiBhbnkpIHtcbiAgICAgICAgbGV0IGRhdGUgPSBtb21lbnQocGF5bG9hZC5ldmVudERhdGUpLmZvcm1hdCgnREQtTU0tWVlZWScpO1xuICAgICAgICBpZiAocGF5bG9hZC5ldmVudFR5cGUgPT09ICdiaXJ0aGRheScpIHtcbiAgICAgICAgICAgIGRhdGUgPSBtb21lbnQocGF5bG9hZC5ldmVudERhdGUpLmZvcm1hdCgnREQtTU0nKTtcbiAgICAgICAgfVxuICAgICAgICBsZXQgcGVyc29uID0gJyc7XG4gICAgICAgIGlmIChwYXlsb2FkICYmIHBheWxvYWQucGVyc29uICYmIHBheWxvYWQucGVyc29uLnBlcnNvbmFsSW5mbykge1xuICAgICAgICAgICAgaWYgKHBheWxvYWQucGVyc29uLnBlcnNvbmFsSW5mby5uYW1lKSB7XG4gICAgICAgICAgICAgICAgcGVyc29uICs9IHBheWxvYWQucGVyc29uLnBlcnNvbmFsSW5mby5uYW1lO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHBheWxvYWQucGVyc29uLnBlcnNvbmFsSW5mby5zdXJuYW1lKSB7XG4gICAgICAgICAgICAgICAgcGVyc29uICs9ICcgJyArIHBheWxvYWQucGVyc29uLnBlcnNvbmFsSW5mby5zdXJuYW1lO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiB7IHBlcnNvbiwgZGF0ZSB9O1xuICAgIH1cblxuICAgIHVwZGF0ZVJlcXVlc3RTdGF0dXMoaGltbmFya0lEOiBzdHJpbmcsIHJlcXVlc3RJRDogc3RyaW5nLCBzdGF0dXM6IHN0cmluZywgcmVxdWVzdDogYW55LCBldmVudCkge1xuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcbiAgICAgICAgdGhpcy5ucy5jaGFuZ2VSZXF1ZXN0U3RhdHVzKGhpbW5hcmtJRCwgcmVxdWVzdElELCBzdGF0dXMsIHJlcXVlc3QpLnN1YnNjcmliZSgocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IG5vdGlmaWNhdGlvbiA9IHRoaXMubm90aWZpY2F0aW9ucy5maW5kKGl0ZW0gPT4gaXRlbS5yZXF1ZXN0T2JqZWN0Ll9pZCA9PT0gcmVzcG9uc2UuaWQpO1xuICAgICAgICAgICAgICAgIGlmIChub3RpZmljYXRpb24pIHtcbiAgICAgICAgICAgICAgICAgICAgbm90aWZpY2F0aW9uLnJlcXVlc3RPYmplY3Quc3RhdHVzID0gcmVzcG9uc2Uuc3RhdHVzO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgfSwgKGVycikgPT4ge1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBnb1RvKGhpbW5hcmtJZDogc3RyaW5nLCBwYXlsb2FkOiBhbnkpIHtcbiAgICAgICAgLy8gTk9URSBpdCBpcyBwb3NzaWJsZSB0byBoYXZlIGRpZmZlcmVudCBwYXRocyBkZXBlbmRzIG9uIGFwcGxpY2F0aW9uXG4gICAgICAgIGlmIChwYXlsb2FkKSB7XG4gICAgICAgICAgICBjb25zdCB0eXBlID0gcGF5bG9hZC50eXBlO1xuICAgICAgICAgICAgbGV0IHBhdGggPSAnLycgKyBoaW1uYXJrSWQgKyAnLyc7XG4gICAgICAgICAgICBpZiAodHlwZSA9PT0gJ3JlcXVlc3QnKSB7XG4gICAgICAgICAgICAgICAgcGF0aCArPSAndGFza3MvJztcbiAgICAgICAgICAgICAgICBwYXRoICs9IHBheWxvYWQucmVxdWVzdFR5cGUgKyAnLyc7XG4gICAgICAgICAgICAgICAgcGF0aCArPSBwYXlsb2FkLnJlcXVlc3ROdW1iZXI7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHR5cGUgPT09ICdldmVudCcpIHtcbiAgICAgICAgICAgICAgICBwYXRoICs9ICdlbXBsb3llZXMvJztcbiAgICAgICAgICAgICAgICBwYXRoICs9IHBheWxvYWQucGVyc29uLl9pZCArICcvJztcbiAgICAgICAgICAgICAgICBjb25zdCBldmVudFR5cGUgPSBwYXlsb2FkLmV2ZW50VHlwZTtcbiAgICAgICAgICAgICAgICAvLyBOT1RFIGtub3duIGV2ZW50IHR5cGVzICdiaXJ0aGRheScsICd0cmlhbFBlcmlvZCcsICd0ZXJtaW5hdGlvbidcbiAgICAgICAgICAgICAgICBpZiAoZXZlbnRUeXBlID09PSAnYmlydGhkYXknKSB7XG4gICAgICAgICAgICAgICAgICAgIHBhdGggKz0gJ2RldGFpbHMnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHBhdGggKz0gJ2NvbnRyYWN0cyc7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgaWRGcm9tVXJsID0gd2luZG93LmxvY2F0aW9uLnBhdGhuYW1lLnNwbGl0KCcvJylbMV07XG4gICAgICAgICAgICBpZiAoaGltbmFya0lkICE9PSBpZEZyb21VcmwpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmhpbW5hcmtDaGFuZ2VkLmVtaXQoaGltbmFya0lkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwocGF0aCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZWFkKCkge1xuICAgICAgICBjb25zdCBub3RSZWFkSURzID0gdGhpcy5ub3RpZmljYXRpb25zLmZpbHRlcihpdGVtID0+ICFpdGVtLnJlYWQpLm1hcChpdGVtID0+IGl0ZW0uX2lkKTtcbiAgICAgICAgaWYgKG5vdFJlYWRJRHMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZGF0YSA9IHtcbiAgICAgICAgICAgIHBheWxvYWQ6IG5vdFJlYWRJRHMsXG4gICAgICAgICAgICBhY3Rpb246ICdyZWFkJ1xuICAgICAgICB9O1xuICAgICAgICB0aGlzLm5zLnVwZGF0ZU5vdGlmaWNhdGlvbnMoZGF0YSkuc3Vic2NyaWJlKCh1cGRhdGVkSURzOiBhbnkpID0+IHtcbiAgICAgICAgICAgIGxldCByZWFkQ291bnQgPSAwO1xuICAgICAgICAgICAgaWYgKEFycmF5LmlzQXJyYXkodXBkYXRlZElEcykpIHtcbiAgICAgICAgICAgICAgICByZWFkQ291bnQgPSB1cGRhdGVkSURzLmxlbmd0aDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuYmFkZ2VOdW1iZXIgLT0gcmVhZENvdW50O1xuICAgICAgICAgICAgY29uc29sZS5sb2coJ3RoaXMuYmFkZ2VOdW1iZScsIHRoaXMuYmFkZ2VOdW1iZXIsIHJlYWRDb3VudCk7XG4gICAgICAgICAgICBpZiAodGhpcy5iYWRnZU51bWJlciA9PT0gMCkge1xuICAgICAgICAgICAgICAgIHRoaXMuaGlkZU1hdEJhZGdlID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5mb3JFYWNoKGl0ZW0gPT4ge1xuICAgICAgICAgICAgICAgIGlmIChub3RSZWFkSURzLmluY2x1ZGVzKGl0ZW0uX2lkKSkge1xuICAgICAgICAgICAgICAgICAgICBpdGVtLnJlYWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbn1cbiJdfQ==