/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { NotificationsComponent } from './notifications/notifications.component';
import { Subject } from 'rxjs';
import { NotificationsService } from './services/notifications.service';
import { SocketService } from './services/socket.service';
import { FormatDatesPipe } from './pipes/format-dates.pipe';
import { NOTIFICATIONS_CONFIG } from './services/config.token';
// REVIEW is this required?
export class Loader {
    constructor() {
        this.translations = new Subject();
        this.$translations = this.translations.asObservable();
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getTranslation(lang) {
        return this.$translations;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    Loader.prototype.translations;
    /** @type {?} */
    Loader.prototype.$translations;
}
/**
 * @return {?}
 */
export function LoaderFactory() {
    return new Loader();
}
// @dynamic
export class NotificationsModule {
}
NotificationsModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule,
                    MatBadgeModule,
                    MatButtonModule,
                    MatMenuModule,
                    MatIconModule,
                    MatDividerModule,
                    FlexLayoutModule,
                    TranslateModule.forChild({
                        loader: {
                            provide: TranslateLoader,
                            // REVIEW  is this required?
                            useFactory: LoaderFactory
                        }
                    })],
                declarations: [NotificationsComponent,
                    FormatDatesPipe],
                exports: [NotificationsComponent,
                    FormatDatesPipe],
                providers: [
                    NotificationsService,
                    SocketService,
                    {
                        provide: NOTIFICATIONS_CONFIG,
                        useFactory: ConfigFactory
                    }
                ]
            },] }
];
if (false) {
    /** @type {?} */
    NotificationsModule.config;
}
/**
 * @return {?}
 */
export function ConfigFactory() {
    return NotificationsModule.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL25vdGlmaWNhdGlvbnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzNELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN6RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzdELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxlQUFlLEVBQ3BCLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRWpELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFFL0IsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRTFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUc1RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQzs7QUFHL0QsTUFBTSxPQUFPLE1BQU07SUFBbkI7UUFDWSxpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsa0JBQWEsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBSXJELENBQUM7Ozs7O0lBSEcsY0FBYyxDQUFDLElBQVk7UUFDekIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQzVCLENBQUM7Q0FDSjs7Ozs7O0lBTEcsOEJBQXFDOztJQUNyQywrQkFBaUQ7Ozs7O0FBTXJELE1BQU0sVUFBVSxhQUFhO0lBQ3pCLE9BQU8sSUFBSSxNQUFNLEVBQUUsQ0FBQztBQUN4QixDQUFDOztBQThCRCxNQUFNLE9BQU8sbUJBQW1COzs7WUEzQi9CLFFBQVEsU0FBQztnQkFDTixPQUFPLEVBQUUsQ0FBQyxZQUFZO29CQUNsQixjQUFjO29CQUNkLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixhQUFhO29CQUNiLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixlQUFlLENBQUMsUUFBUSxDQUFDO3dCQUNyQixNQUFNLEVBQUU7NEJBQ0osT0FBTyxFQUFFLGVBQWU7OzRCQUN4QixVQUFVLEVBQUUsYUFBYTt5QkFDNUI7cUJBQ0osQ0FBQyxDQUFDO2dCQUNQLFlBQVksRUFBRSxDQUFDLHNCQUFzQjtvQkFDakMsZUFBZSxDQUFDO2dCQUNwQixPQUFPLEVBQUUsQ0FBQyxzQkFBc0I7b0JBQzVCLGVBQWUsQ0FBQztnQkFDcEIsU0FBUyxFQUFFO29CQUNQLG9CQUFvQjtvQkFDcEIsYUFBYTtvQkFDYjt3QkFDSSxPQUFPLEVBQUUsb0JBQW9CO3dCQUM3QixVQUFVLEVBQUUsYUFBYTtxQkFDNUI7aUJBQ0o7YUFDSjs7OztJQUVHLDJCQUFtQzs7Ozs7QUFHdkMsTUFBTSxVQUFVLGFBQWE7SUFDekIsT0FBTyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7QUFDdEMsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgTWF0QnV0dG9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYnV0dG9uJztcbmltcG9ydCB7IE1hdEJhZGdlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvYmFkZ2UnO1xuaW1wb3J0IHsgTWF0TWVudU1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL21lbnUnO1xuaW1wb3J0IHsgTWF0SWNvbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2ljb24nO1xuaW1wb3J0IHsgTWF0RGl2aWRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpdmlkZXInO1xuaW1wb3J0IHsgRmxleExheW91dE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcblxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlLFxuICAgIFRyYW5zbGF0ZUxvYWRlciB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xuXG5pbXBvcnQgeyBOb3RpZmljYXRpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9ub3RpZmljYXRpb25zL25vdGlmaWNhdGlvbnMuY29tcG9uZW50JztcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcblxuaW1wb3J0IHsgTm90aWZpY2F0aW9uc1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL25vdGlmaWNhdGlvbnMuc2VydmljZSc7XG5pbXBvcnQgeyBTb2NrZXRTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9zb2NrZXQuc2VydmljZSc7XG5cbmltcG9ydCB7IEZvcm1hdERhdGVzUGlwZSB9IGZyb20gJy4vcGlwZXMvZm9ybWF0LWRhdGVzLnBpcGUnO1xuXG5pbXBvcnQgeyBOb3RpZmljYXRpb25zQ29uZmlnIH0gZnJvbSAnLi9tb2RlbHMvY29uZmlnJztcbmltcG9ydCB7IE5PVElGSUNBVElPTlNfQ09ORklHIH0gZnJvbSAnLi9zZXJ2aWNlcy9jb25maWcudG9rZW4nO1xuXG4vLyBSRVZJRVcgaXMgdGhpcyByZXF1aXJlZD9cbmV4cG9ydCBjbGFzcyBMb2FkZXIgaW1wbGVtZW50cyBUcmFuc2xhdGVMb2FkZXIge1xuICAgIHByaXZhdGUgdHJhbnNsYXRpb25zID0gbmV3IFN1YmplY3QoKTtcbiAgICAkdHJhbnNsYXRpb25zID0gdGhpcy50cmFuc2xhdGlvbnMuYXNPYnNlcnZhYmxlKCk7XG4gICAgZ2V0VHJhbnNsYXRpb24obGFuZzogc3RyaW5nKSB7XG4gICAgICByZXR1cm4gdGhpcy4kdHJhbnNsYXRpb25zO1xuICAgIH1cbn1cblxuZXhwb3J0IGZ1bmN0aW9uIExvYWRlckZhY3RvcnkoKSB7XG4gICAgcmV0dXJuIG5ldyBMb2FkZXIoKTtcbn1cblxuLy8gQGR5bmFtaWNcbkBOZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0NvbW1vbk1vZHVsZSxcbiAgICAgICAgTWF0QmFkZ2VNb2R1bGUsXG4gICAgICAgIE1hdEJ1dHRvbk1vZHVsZSxcbiAgICAgICAgTWF0TWVudU1vZHVsZSxcbiAgICAgICAgTWF0SWNvbk1vZHVsZSxcbiAgICAgICAgTWF0RGl2aWRlck1vZHVsZSxcbiAgICAgICAgRmxleExheW91dE1vZHVsZSxcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKHtcbiAgICAgICAgICAgIGxvYWRlcjoge1xuICAgICAgICAgICAgICAgIHByb3ZpZGU6IFRyYW5zbGF0ZUxvYWRlciwgLy8gUkVWSUVXICBpcyB0aGlzIHJlcXVpcmVkP1xuICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IExvYWRlckZhY3RvcnlcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSldLFxuICAgIGRlY2xhcmF0aW9uczogW05vdGlmaWNhdGlvbnNDb21wb25lbnQsXG4gICAgICAgIEZvcm1hdERhdGVzUGlwZV0sXG4gICAgZXhwb3J0czogW05vdGlmaWNhdGlvbnNDb21wb25lbnQsXG4gICAgICAgIEZvcm1hdERhdGVzUGlwZV0sXG4gICAgcHJvdmlkZXJzOiBbXG4gICAgICAgIE5vdGlmaWNhdGlvbnNTZXJ2aWNlLFxuICAgICAgICBTb2NrZXRTZXJ2aWNlLFxuICAgICAgICB7XG4gICAgICAgICAgICBwcm92aWRlOiBOT1RJRklDQVRJT05TX0NPTkZJRyxcbiAgICAgICAgICAgIHVzZUZhY3Rvcnk6IENvbmZpZ0ZhY3RvcnlcbiAgICAgICAgfVxuICAgIF1cbn0pXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uc01vZHVsZSB7XG4gICAgc3RhdGljIGNvbmZpZzogTm90aWZpY2F0aW9uc0NvbmZpZztcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIENvbmZpZ0ZhY3RvcnkoKSB7XG4gICAgcmV0dXJuIE5vdGlmaWNhdGlvbnNNb2R1bGUuY29uZmlnO1xufVxuIl19