/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import io from 'socket.io-client';
import { NOTIFICATIONS_CONFIG } from './config.token';
export class SocketService {
    /**
     * @param {?} notificationsConfig
     */
    constructor(notificationsConfig) {
        this.notificationsConfig = notificationsConfig;
    }
    // // NOTE it will be nice to create connection in constructor
    /**
     * @param {?} auth0ID
     * @return {?}
     */
    createConnection(auth0ID) {
        this.socket = io(this.notificationsConfig.socketAPI, {
            transports: ['websocket'],
            query: { auth0ID }
        });
    }
    // HANDLER
    /**
     * @return {?}
     */
    onStatusUpdate() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('progressUpdate', (/**
             * @param {?} msg
             * @return {?}
             */
            msg => {
                observer.next(msg);
            }));
        }));
    }
    /**
     * @return {?}
     */
    onEventAdded() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('eventAdded', (/**
             * @param {?} msg
             * @return {?}
             */
            msg => {
                observer.next(msg);
            }));
        }));
    }
    /**
     * @return {?}
     */
    onNewNotification() {
        return Observable.create((/**
         * @param {?} observer
         * @return {?}
         */
        observer => {
            this.socket.on('newNotification', (/**
             * @param {?} msg
             * @return {?}
             */
            (msg) => {
                observer.next(JSON.parse(msg));
            }));
        }));
    }
    /**
     * @param {?} eventData
     * @return {?}
     */
    addEvent(eventData) {
        this.socket.send('addEvent', eventData);
    }
}
SocketService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SocketService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [NOTIFICATIONS_CONFIG,] }] }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.socket;
    /** @type {?} */
    SocketService.prototype.profile;
    /**
     * @type {?}
     * @private
     */
    SocketService.prototype.notificationsConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ja2V0LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AaGltbmFyay9ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsibGliL3NlcnZpY2VzL3NvY2tldC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRWxDLE9BQU8sRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRWxDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBR3RELE1BQU0sT0FBTyxhQUFhOzs7O0lBSXRCLFlBQzBDLG1CQUF3QztRQUF4Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQy9FLENBQUM7Ozs7OztJQUdKLGdCQUFnQixDQUFDLE9BQWU7UUFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsRUFBRTtZQUNqRCxVQUFVLEVBQUUsQ0FBQyxXQUFXLENBQUM7WUFDekIsS0FBSyxFQUFFLEVBQUUsT0FBTyxFQUFFO1NBQ3JCLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBR0QsY0FBYztRQUNWLE9BQU8sVUFBVSxDQUFDLE1BQU07Ozs7UUFBQyxRQUFRLENBQUMsRUFBRTtZQUNoQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0I7Ozs7WUFBRSxHQUFHLENBQUMsRUFBRTtnQkFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QixDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLFVBQVUsQ0FBQyxNQUFNOzs7O1FBQUMsUUFBUSxDQUFDLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsWUFBWTs7OztZQUFFLEdBQUcsQ0FBQyxFQUFFO2dCQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2IsT0FBTyxVQUFVLENBQUMsTUFBTTs7OztRQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLGlCQUFpQjs7OztZQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ3RDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ25DLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxTQUFjO1FBQ25CLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7WUE1Q0osVUFBVTs7Ozs0Q0FNRixNQUFNLFNBQUMsb0JBQW9COzs7Ozs7O0lBSmhDLCtCQUEwQjs7SUFDMUIsZ0NBQWE7Ozs7O0lBR1QsNENBQThFIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5cbmltcG9ydCBpbyBmcm9tICdzb2NrZXQuaW8tY2xpZW50JztcbmltcG9ydCB7IE5vdGlmaWNhdGlvbnNDb25maWcgfSBmcm9tICcuLi9tb2RlbHMvY29uZmlnJztcbmltcG9ydCB7IE5PVElGSUNBVElPTlNfQ09ORklHIH0gZnJvbSAnLi9jb25maWcudG9rZW4nO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU29ja2V0U2VydmljZSB7XG4gICAgcHJpdmF0ZSBzb2NrZXQ6IGlvLlNvY2tldDtcbiAgICBwcm9maWxlOiBhbnk7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgQEluamVjdChOT1RJRklDQVRJT05TX0NPTkZJRykgcHJpdmF0ZSBub3RpZmljYXRpb25zQ29uZmlnOiBOb3RpZmljYXRpb25zQ29uZmlnXG4gICAgKSB7fVxuXG4gICAgLy8gLy8gTk9URSBpdCB3aWxsIGJlIG5pY2UgdG8gY3JlYXRlIGNvbm5lY3Rpb24gaW4gY29uc3RydWN0b3JcbiAgICBjcmVhdGVDb25uZWN0aW9uKGF1dGgwSUQ6IHN0cmluZykge1xuICAgICAgICB0aGlzLnNvY2tldCA9IGlvKHRoaXMubm90aWZpY2F0aW9uc0NvbmZpZy5zb2NrZXRBUEksIHtcbiAgICAgICAgICAgIHRyYW5zcG9ydHM6IFsnd2Vic29ja2V0J10sXG4gICAgICAgICAgICBxdWVyeTogeyBhdXRoMElEIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgLy8gSEFORExFUlxuICAgIG9uU3RhdHVzVXBkYXRlKCkge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5jcmVhdGUob2JzZXJ2ZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5zb2NrZXQub24oJ3Byb2dyZXNzVXBkYXRlJywgbXNnID0+IHtcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KG1zZyk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgb25FdmVudEFkZGVkKCkge1xuICAgICAgICByZXR1cm4gT2JzZXJ2YWJsZS5jcmVhdGUob2JzZXJ2ZXIgPT4ge1xuICAgICAgICAgICAgdGhpcy5zb2NrZXQub24oJ2V2ZW50QWRkZWQnLCBtc2cgPT4ge1xuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQobXNnKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBvbk5ld05vdGlmaWNhdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIE9ic2VydmFibGUuY3JlYXRlKG9ic2VydmVyID0+IHtcbiAgICAgICAgICAgIHRoaXMuc29ja2V0Lm9uKCduZXdOb3RpZmljYXRpb24nLCAobXNnKSA9PiB7XG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChKU09OLnBhcnNlKG1zZykpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGFkZEV2ZW50KGV2ZW50RGF0YTogYW55KSB7XG4gICAgICAgIHRoaXMuc29ja2V0LnNlbmQoJ2FkZEV2ZW50JywgZXZlbnREYXRhKTtcbiAgICB9XG59XG4iXX0=